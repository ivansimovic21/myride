package ivan.ba.myride.util.options

import ivan.ba.myride.data.dataModels.DMLoggedUser

object MyOptions {
    val loggedUser: Option<DMLoggedUser> = Option("loggedUser", "", DMLoggedUser::class.java)
}


package ivan.ba.myride.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder

object MyGson {
    fun build(): Gson {
        val builder = GsonBuilder()
        return builder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
    }

    fun buildLicensa(): Gson {
        val builder = GsonBuilder()
        return builder.setDateFormat("dd.MM.yyyy.").create()
    }

}

package ivan.ba.myride.util.options

import android.content.Context
import ivan.ba.myride.MyApp
import ivan.ba.myride.util.MyGson
import java.io.Serializable

class Option<T>(var Name: String, var Default: String, var Type:Class<T>) : Serializable {

    fun GET(): T? {
        val preferences = MyApp.instance.getContext().getSharedPreferences("MyApp", Context.MODE_PRIVATE)
        val value = preferences.getString(Name, Default) ?: return null
        try {
            return MyGson.build().fromJson(value, Type)
        } catch (e: Exception) {
            return null
        }
    }

    fun SET(value: T) {
        val preferences = MyApp.instance.getContext().getSharedPreferences("MyApp", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString(Name, MyGson.build().toJson(value))
        editor.commit()
    }
}
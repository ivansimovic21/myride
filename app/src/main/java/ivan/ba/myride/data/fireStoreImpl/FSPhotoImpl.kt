package ivan.ba.myride.data.fireStoreImpl

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoInsert
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment

interface FSPhotoImpl{

    fun insertPhoto(photo: DMPhotoInsert, onFinish: (String?) -> Unit)
    fun getPhotosByCarID(carID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
    fun getPhotosForExplore(onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
    fun getPhotosByUserID(userID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
    fun getPhotoByID(carID: String, onFinish: (DMPhoto) -> Unit)
    fun getCommentsByPhotoID(photoID: String, onFinish: (ArrayList<DMComment>?) -> Unit)
    fun insertComment(item: DMComment, onFinish: (String?) -> Unit)
    fun updateComment(item: DMComment, onFinish: (Boolean) -> Unit)
    fun deleteCommentByID(photoID: String, commentID: String, onFinish: (Boolean) -> Unit)
    fun updateUserImagePath(userID: String, imagePath: String,onFinish: (Boolean) -> Unit)
}
package ivan.ba.myride.data.fireStore

import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.firestore.FirebaseFirestore
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarInsert
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.fireStoreImpl.FSCarImpl

class FSCar : FSCarImpl {
    val myDB = FirebaseFirestore.getInstance()
    val collectionCars = myDB.collection("cars")
    val collectionPhotos = myDB.collection("photos")

    //region Cars

    override fun getCarsByUserID(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit) {
        val list = ArrayList<DMCarProfile>()
        collectionCars.whereEqualTo("userID", userID).get().addOnSuccessListener {

            it.forEach {
                list.add(DMCarProfile(it.id, it.getString("userID")!!, it.getString("name")!!, it.getString("imagePath")!!))
            }
            onFinish(list)
        }.addOnFailureListener(OnFailureListener {
            onFinish(null)
        })
    }

    override fun getCarByID(CarID: String, onFinish: (DMCar?) -> Unit) {
        collectionCars.document(CarID).get().addOnSuccessListener {
            if (it.exists()) {
                val item = DMCar(it.id, it.getString("userID")!!, it.getString("name")!!, it.getString("imagePath")!!)
                onFinish(item)
            }
        }.addOnFailureListener(OnFailureListener {
            onFinish(null)
        })
    }

    override fun insertCar(item: DMCarInsert, onFinish: (String?) -> Unit) {
        collectionCars.add(mapOf(
                "userID" to item.userID,
                "name" to item.name,
                "imagePath" to ""
        )).addOnSuccessListener {
            onFinish(it.id)
        }.addOnFailureListener {
            onFinish(null)
        }
    }

    override fun updateCar(item: DMCar, onFinish: (Boolean) -> Unit) {
        collectionCars.document(item.carID).update(mapOf(
                "name" to item.name
        )).addOnSuccessListener {
            onFinish(true)
        }.addOnFailureListener {
            onFinish(false)
        }
    }

    override fun deleteCarByID(id: String, onFinish: (Boolean) -> Unit) {
        collectionCars.document(id).delete().addOnSuccessListener {
            onFinish(true)
        }.addOnFailureListener {
            onFinish(false)
        }
    }

    override fun updateCarImagePath(carID: String, imagePath: String, onFinish: (Boolean) -> Unit) {
        collectionPhotos.whereEqualTo("carID",carID).get().addOnSuccessListener {
            onFinish(true)
            it.documents.forEach {
                it.reference.update(mapOf("carImagePath" to imagePath))
            }
            val car = collectionCars.document(carID)
            car.update(mapOf("imagePath" to imagePath)).addOnSuccessListener {
            }
        }.addOnFailureListener {
            onFinish(false)
        }
    }

    //endregion

}
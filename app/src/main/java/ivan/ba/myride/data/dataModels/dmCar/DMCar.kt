package ivan.ba.myride.data.dataModels.dmCar

data class DMCar(var carID: String, var userID: String, var name: String, var imagePath: String)
package ivan.ba.myride.data.fireStoreImpl

import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto

interface FSProfilePhotosImpl{

    fun getUserProfilePictures(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit)
    fun insertUserProfilePictures(item: DMProfilePhoto, onFinish: (String?) -> Unit)
    fun getCarProfilePictures(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit)
    fun insertCarProfilePictures(item: DMProfilePhoto, onFinish: (String?) -> Unit)
}
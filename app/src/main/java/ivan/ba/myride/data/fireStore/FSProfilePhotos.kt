package ivan.ba.myride.data.fireStore

import com.google.firebase.firestore.FirebaseFirestore
import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto
import ivan.ba.myride.data.fireStoreImpl.FSProfilePhotosImpl

class FSProfilePhotos : FSProfilePhotosImpl {
    val myDB = FirebaseFirestore.getInstance()
    val collectionCars = myDB.collection("cars")
    val collectionUsers = myDB.collection("users")


    override fun getUserProfilePictures(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit) {
        val list = ArrayList<DMProfilePhoto>()
        collectionUsers.document(userID).collection("user_profile_pictures").get().addOnSuccessListener {

            it.forEach {
                list.add(DMProfilePhoto(it.id, it.getString("ref_id")!!, it.getString("imagePath")!!))
            }
            onFinish(list)
        }.addOnFailureListener{
            onFinish(null)
        }
    }

    override fun insertUserProfilePictures(item: DMProfilePhoto, onFinish: (String?) -> Unit) {
        val collectionOfProfilePictures = collectionUsers.document(item.ref_id).collection("user_profile_pictures")
        collectionOfProfilePictures.add(mapOf(
                "ref_id" to item.ref_id,
                "imagePath" to item.imagePath
        )).addOnSuccessListener {

            onFinish(it.id)
        }.addOnFailureListener {
            onFinish(null)
        }
    }

    override fun getCarProfilePictures(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit) {
        val collectionOfProfilePictures = ArrayList<DMProfilePhoto>()
        collectionCars.document(userID).collection("car_profile_pictures").get().addOnSuccessListener {

            it.forEach {
                collectionOfProfilePictures.add(DMProfilePhoto(it.id, it.getString("ref_id")!!, it.getString("imagePath")!!))
            }
            onFinish(collectionOfProfilePictures)
        }.addOnFailureListener{
            onFinish(null)
        }
    }

    override fun insertCarProfilePictures(item: DMProfilePhoto, onFinish: (String?) -> Unit) {
        val collectionOfProfilePictures = collectionCars.document(item.ref_id).collection("car_profile_pictures")
        collectionOfProfilePictures.add(mapOf(
                "ref_id" to item.ref_id,
                "imagePath" to item.imagePath
        )).addOnSuccessListener {
            onFinish(it.id)
        }.addOnFailureListener {
            onFinish(null)
        }
    }
}
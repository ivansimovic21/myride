package ivan.ba.myride.data.reposetory

import ivan.ba.myride.MyApp
import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto
import ivan.ba.myride.data.fireStoreImpl.FSCarImpl
import ivan.ba.myride.data.fireStoreImpl.FSPhotoImpl
import ivan.ba.myride.data.fireStoreImpl.FSProfilePhotosImpl
import ivan.ba.myride.data.reposetoryImpl.RepProfilePhotoImpl
import javax.inject.Inject

class RepProfilePhoto() : RepProfilePhotoImpl {

    init {
        MyApp.graphRepository.inject(this)
    }

    @Inject
    lateinit var fs: FSProfilePhotosImpl
    @Inject
    lateinit var fsPhoto: FSPhotoImpl
    @Inject
    lateinit var fsCar: FSCarImpl
    override fun getUserProfilePictures(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit) {
        fs.getUserProfilePictures(userID, onFinish)
    }

    override fun insertUserProfilePictures(item: DMProfilePhoto, onFinish: (Boolean) -> Unit) {
        fs.insertUserProfilePictures(item){
            fsPhoto.updateUserImagePath(item.ref_id,item.imagePath,onFinish)
        }
    }

    override fun getCarProfilePictures(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit) {
        fs.getCarProfilePictures(userID, onFinish)
    }

    override fun insertCarProfilePictures(item: DMProfilePhoto, onFinish: (Boolean) -> Unit) {
        fs.insertCarProfilePictures(item){
            fsCar.updateCarImagePath(item.ref_id,item.imagePath,onFinish)
        }
    }

}
package ivan.ba.myride.data.dataModels.dmPhoto

import android.os.Parcel
import android.os.Parcelable

data class DMPhotoInsert(
        var userID: String = "",
        var userName: String = "",
        var userImagePath: String = "",
        var carID: String? = null,
        var carName: String? = null,
        var carImagePath: String? = null,
        var carBrandID: String? = null,
        var carBrandName: String? = null,
        var imagePath: String = "",
        var imageDescription: String = "",
        var likes: Int = 0,
        var comments: Int = 0

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userID)
        parcel.writeString(userName)
        parcel.writeString(userImagePath)
        parcel.writeString(carID)
        parcel.writeString(carName)
        parcel.writeString(carImagePath)
        parcel.writeString(carBrandID)
        parcel.writeString(carBrandName)
        parcel.writeString(imagePath)
        parcel.writeString(imageDescription)
        parcel.writeInt(likes)
        parcel.writeInt(comments)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DMPhotoInsert> {
        override fun createFromParcel(parcel: Parcel): DMPhotoInsert {
            return DMPhotoInsert(parcel)
        }

        override fun newArray(size: Int): Array<DMPhotoInsert?> {
            return arrayOfNulls(size)
        }
    }
}
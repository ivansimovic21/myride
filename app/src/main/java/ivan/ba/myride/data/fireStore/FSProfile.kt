package ivan.ba.myride.data.fireStore

import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.firestore.FirebaseFirestore
import ivan.ba.myride.data.dataModels.dmProfile.DMProfile
import ivan.ba.myride.data.fireStoreImpl.FSProfileImpl

class FSProfile : FSProfileImpl {

    val myDB = FirebaseFirestore.getInstance()
    val collectionUsers = myDB.collection("users")

    override fun getProfileByID(userID: String, onFinish: (DMProfile?) -> Unit) {
        collectionUsers.document(userID).get().addOnSuccessListener {
            if (it.exists()) {
                val item = DMProfile(it.id, it.getString("profilePicturePath")!!, it.getString("name")!!, it.getLong("carsCount")!!, it.getLong("photoCount")!!, it.getLong("followers")!!, it.getLong("following")!!)
                onFinish(item)
            }
        }.addOnFailureListener(OnFailureListener {
            onFinish(null)
        })
    }
}
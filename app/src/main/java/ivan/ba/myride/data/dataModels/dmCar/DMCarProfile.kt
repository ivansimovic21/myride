package ivan.ba.myride.data.dataModels.dmCar

import android.os.Parcel
import android.os.Parcelable

data class DMCarProfile(var carID: String, var userID: String, var name: String, var imagePath: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(carID)
        parcel.writeString(userID)
        parcel.writeString(name)
        parcel.writeString(imagePath)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DMCarProfile> {
        override fun createFromParcel(parcel: Parcel): DMCarProfile {
            return DMCarProfile(parcel)
        }

        override fun newArray(size: Int): Array<DMCarProfile?> {
            return arrayOfNulls(size)
        }
    }
}
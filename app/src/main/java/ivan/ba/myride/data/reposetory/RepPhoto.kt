package ivan.ba.myride.data.reposetory

import ivan.ba.myride.MyApp
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoInsert
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment
import ivan.ba.myride.data.fireStoreImpl.FSPhotoImpl
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl
import javax.inject.Inject

class RepPhoto : RepPhotoImpl {
    @Inject
    lateinit var fs: FSPhotoImpl

    init {
        MyApp.graphRepository.inject(this)
    }

    override fun insertPhoto(photo: DMPhotoInsert, onFinish: (String?) -> Unit) {
        fs.insertPhoto(photo, onFinish)
    }

    override fun getPhotoByID(photoID: String, onFinish: (DMPhoto?) -> Unit) {
        fs.getPhotoByID(photoID, onFinish)
    }

    override fun getPhotosByUserID(userID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        fs.getPhotosByUserID(userID, onFinish)
    }

    override fun getPhotosByCarID(carID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        fs.getPhotosByCarID(carID, onFinish)
    }

    override fun getPhotosForExplore(onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        fs.getPhotosForExplore(onFinish)
    }

    override fun insertComment(photo: DMComment, onFinish: (String?) -> Unit) {
        fs.insertComment(photo, onFinish)
    }

    override fun updateComment(photo: DMComment, onFinish: (Boolean) -> Unit) {
        fs.updateComment(photo, onFinish)
    }

    override fun getCommentsByPhotoID(photoID: String, onFinish: (ArrayList<DMComment>?) -> Unit) {
        fs.getCommentsByPhotoID(photoID, onFinish)
    }

    override fun deleteComment(photoID: String, commentID: String, onFinish: (Boolean) -> Unit) {
        fs.deleteCommentByID(photoID, commentID, onFinish)
    }

}
package ivan.ba.myride.data.reposetory

import ivan.ba.myride.MyApp
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarInsert
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.data.fireStoreImpl.FSCarImpl
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl
import javax.inject.Inject

class RepCar : RepCarImpl {
    @Inject
    lateinit var fs: FSCarImpl

    init {
        MyApp.graphRepository.inject(this)
    }

    override fun getCarsByUserID(userID:String,onFinish: (ArrayList<DMCarProfile>?) -> Unit) {
        fs.getCarsByUserID(userID,onFinish)
    }

    override fun getCarByID(carID: String, onFinish: (DMCar?) -> Unit) {
        fs.getCarByID(carID, onFinish)
    }



    override fun insertCar(item: DMCarInsert, onFinish: (String?) -> Unit) {
        fs.insertCar(item, onFinish)
    }

    override fun updateCar(item: DMCar, onFinish: (Boolean) -> Unit) {
        fs.updateCar(item, onFinish)
    }

    override fun deleteCarByID(id: String, onFinish: (Boolean) -> Unit) {
        fs.deleteCarByID(id, onFinish)
    }
}
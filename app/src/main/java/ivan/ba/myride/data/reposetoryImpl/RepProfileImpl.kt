package ivan.ba.myride.data.reposetoryImpl

import ivan.ba.myride.data.dataModels.dmProfile.DMProfile

interface RepProfileImpl{
    fun getProfileByID(userID: String, onFinish: (DMProfile?) -> Unit)
}
package ivan.ba.myride.data.reposetoryImpl

import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto

interface RepProfilePhotoImpl{
    fun getUserProfilePictures(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit)
    fun insertUserProfilePictures(item: DMProfilePhoto, onFinish: (Boolean) -> Unit)
    fun getCarProfilePictures(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit)
    fun insertCarProfilePictures(item: DMProfilePhoto, onFinish: (Boolean) -> Unit)
}
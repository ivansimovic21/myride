package ivan.ba.myride.data.fireStore

import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.firestore.FirebaseFirestore
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarInsert
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoInsert
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment
import ivan.ba.myride.data.fireStoreImpl.FSPhotoImpl

class FSPhoto : FSPhotoImpl {
    val myDB = FirebaseFirestore.getInstance()
    val collectionPhotos = myDB.collection("photos")
    val collectionUsers = myDB.collection("users")

    override fun insertPhoto(photo: DMPhotoInsert, onFinish: (String?) -> Unit) {
        collectionPhotos.add(mapOf(
                "userID" to photo.userID,
                "userName" to photo.userName,
                "userImagePath" to photo.userImagePath,
                "carID" to photo.carID,
                "carName" to photo.carName,
                "carImagePath" to photo.carImagePath,
                "carBrandID" to photo.carBrandID,
                "carBrandName" to photo.carBrandName,
                "imagePath" to photo.imagePath,
                "imageDescription" to photo.imageDescription,
                "likes" to photo.likes,
                "comments" to photo.comments
        )).addOnSuccessListener {
            onFinish(it.id)
        }.addOnFailureListener {
            onFinish(null)
        }
    }

    override fun getPhotoByID(carID: String, onFinish: (DMPhoto) -> Unit) {
        collectionPhotos.document(carID).get().addOnSuccessListener {
            onFinish(it.toObject(DMPhoto::class.java)!!)
        }.addOnFailureListener {

        }
    }

    override fun getPhotosByCarID(carID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        val list = ArrayList<DMPhotoExplore>()
        collectionPhotos.whereEqualTo("carID", carID).get().addOnSuccessListener {

            it.forEach {
                list.add(DMPhotoExplore(it.id, it.getString("carID"), it.getString("imagePath")!!, it.getString("imageDescription")!!))
            }
            onFinish(list)
        }.addOnFailureListener(OnFailureListener {
            onFinish(null)
        })
    }

    override fun getPhotosByUserID(userID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        val list = ArrayList<DMPhotoExplore>()
        collectionPhotos.whereEqualTo("userID", userID).get().addOnSuccessListener {

            it.forEach {
                list.add(DMPhotoExplore(it.id, it.getString("carID"), it.getString("imagePath")!!, it.getString("imageDescription")!!))
            }
            onFinish(list)
        }.addOnFailureListener(OnFailureListener {
            onFinish(null)
        })
    }

    override fun getPhotosForExplore(onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        val list = ArrayList<DMPhotoExplore>()
        collectionPhotos.get().addOnSuccessListener {

            it.forEach {
                list.add(DMPhotoExplore(it.id, it.getString("carID"), it.getString("imagePath")!!, it.getString("imageDescription")!!))
            }
            onFinish(list)
        }.addOnFailureListener(OnFailureListener {
            onFinish(null)
        })
    }


    override fun getCommentsByPhotoID(photoID: String, onFinish: (ArrayList<DMComment>?) -> Unit) {
        val list = ArrayList<DMComment>()
        collectionPhotos.document(photoID).collection("comments").whereEqualTo("photoID", photoID).get().addOnSuccessListener {

            it.forEach {
                list.add(DMComment(it.id, it.getString("photoID")!!, it.getString("userID")!!, it.getString("userName")!!, it.getString("userImagePath")!!, it.getString("commentText")!!))
            }
            onFinish(list)
        }.addOnFailureListener(OnFailureListener {
            onFinish(null)
        })
    }

    override fun insertComment(item: DMComment, onFinish: (String?) -> Unit) {
        val collectionOfComments = collectionPhotos.document(item.photoID).collection("comments")
        collectionOfComments.add(mapOf(
                "photoID" to item.photoID,
                "userID" to item.userID,
                "userName" to item.userName,
                "userImagePath" to item.userImagePath,
                "commentText" to item.commentText
        )).addOnSuccessListener {
            onFinish(it.id)
        }.addOnFailureListener {
            onFinish(null)
        }
    }

    override fun updateComment(item: DMComment, onFinish: (Boolean) -> Unit) {
        val collectionOfComments = collectionPhotos.document(item.photoID).collection("comments")
        collectionOfComments.document(item.commentID).set(mapOf(
                "photoID" to item.photoID,
                "userID" to item.userID,
                "userName" to item.userName,
                "userImagePath" to item.userImagePath,
                "commentText" to item.commentText
        )).addOnSuccessListener {
            onFinish(true)
        }.addOnFailureListener {
            onFinish(false)
        }
    }

    override fun deleteCommentByID(photoID: String, commentID: String, onFinish: (Boolean) -> Unit) {
        val collectionOfComments = collectionPhotos.document(photoID).collection("comments")
        collectionOfComments.document(commentID).delete().addOnSuccessListener {
            onFinish(true)
        }.addOnFailureListener {
            onFinish(false)
        }
    }

    override fun updateUserImagePath(userID: String, imagePath: String, onFinish: (Boolean) -> Unit) {
        collectionPhotos.get().addOnSuccessListener {
            onFinish(true)
            it.forEach {
                val tempUserID = it.getString("userID")
                if(tempUserID == userID)
                    it.reference.update(mapOf("userImagePath" to imagePath))
                collectionPhotos.document(it.id).collection("comments").whereEqualTo("userID", userID).get().addOnSuccessListener {
                    it.documents.forEach {
                        it.reference.update(mapOf("userImagePath" to imagePath))
                    }
                    val user = collectionUsers.document(userID)
                    user.update(mapOf("profilePicturePath" to imagePath)).addOnSuccessListener {
                    }
                }
            }
        }.addOnFailureListener {
            onFinish(false)
        }
    }

}
package ivan.ba.myride.data.fireStore

import dagger.Module
import dagger.Provides
import ivan.ba.myride.data.fireStoreImpl.FSCarImpl
import ivan.ba.myride.data.fireStoreImpl.FSPhotoImpl
import ivan.ba.myride.data.fireStoreImpl.FSProfileImpl
import ivan.ba.myride.data.fireStoreImpl.FSProfilePhotosImpl
import javax.inject.Singleton

@Module
class FireStoreModule() {

    @Provides
    @Singleton
    fun provideFSCar(): FSCarImpl {
        return FSCar()
    }
    @Provides
    @Singleton
    fun provideFSPhoto(): FSPhotoImpl {
        return FSPhoto()
    }
    @Provides
    @Singleton
    fun provideFSProfilePhotos(): FSProfilePhotosImpl {
        return FSProfilePhotos()
    }
    @Provides
    @Singleton
    fun provideFSProfile(): FSProfileImpl {
        return FSProfile()
    }
}
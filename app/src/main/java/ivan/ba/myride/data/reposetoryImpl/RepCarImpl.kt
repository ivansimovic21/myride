package ivan.ba.myride.data.reposetoryImpl

import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarInsert
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile

interface RepCarImpl {

    fun getCarsByUserID(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit)
    fun getCarByID(carID: String, onFinish: (DMCar?) -> Unit)
    fun insertCar(item: DMCarInsert, onFinish: (String?) -> Unit)
    fun updateCar(item: DMCar, onFinish: (Boolean) -> Unit)
    fun deleteCarByID(id: String, onFinish: (Boolean) -> Unit)
}
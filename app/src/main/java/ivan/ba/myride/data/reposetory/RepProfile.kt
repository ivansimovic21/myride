package ivan.ba.myride.data.reposetory

import ivan.ba.myride.MyApp
import ivan.ba.myride.MyApp_MembersInjector
import ivan.ba.myride.data.dataModels.dmProfile.DMProfile
import ivan.ba.myride.data.fireStoreImpl.FSProfileImpl
import ivan.ba.myride.data.reposetoryImpl.RepProfileImpl
import javax.inject.Inject

class RepProfile : RepProfileImpl {

    @Inject
    lateinit var fs: FSProfileImpl

    init {
        MyApp.graphRepository.inject(this)
    }

    override fun getProfileByID(userID: String, onFinish: (DMProfile?) -> Unit) {
        fs.getProfileByID(userID, onFinish)
    }

}
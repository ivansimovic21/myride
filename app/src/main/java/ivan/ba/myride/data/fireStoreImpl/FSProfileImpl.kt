package ivan.ba.myride.data.fireStoreImpl

import ivan.ba.myride.data.dataModels.dmProfile.DMProfile

interface FSProfileImpl{

    fun getProfileByID(userID: String, onFinish: (DMProfile?) -> Unit)
}
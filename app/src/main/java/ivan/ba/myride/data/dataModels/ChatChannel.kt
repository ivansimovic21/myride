package ivan.ba.myride.data.dataModels

data class ChatChannel(val userIds: MutableList<String>) {
    constructor() : this(mutableListOf())
}
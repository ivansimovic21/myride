package ivan.ba.myride.data.dataModels.dmProfilePicture

data class DMProfilePhoto(var id: String, var ref_id: String, var imagePath: String)
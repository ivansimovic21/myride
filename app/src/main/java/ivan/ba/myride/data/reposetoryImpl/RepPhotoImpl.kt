package ivan.ba.myride.data.reposetoryImpl

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoInsert
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment

interface RepPhotoImpl {
    fun insertPhoto(photo: DMPhotoInsert, onFinish: (String?) -> Unit)
    fun getPhotosByUserID(userID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
    fun getPhotosByCarID(carID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
    fun getPhotosForExplore(onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
    fun getPhotoByID(photoID: String, onFinish: (DMPhoto?) -> Unit)
    fun getCommentsByPhotoID(photoID: String, onFinish: (ArrayList<DMComment>?) -> Unit)
    fun insertComment(photo: DMComment, onFinish: (String?) -> Unit)
    fun deleteComment(photoID: String, commentID: String, onFinish: (Boolean) -> Unit)
    fun updateComment(photo: DMComment, onFinish: (Boolean) -> Unit)
}
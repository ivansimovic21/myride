package ivan.ba.myride.data.dataModels.dmPhoto.dmComment

import java.io.Serializable

data class DMComment(
        var commentID: String = "",
        var photoID: String = "",
        var userID: String = "",
        var userName: String = "",
        var userImagePath: String = "",
        var commentText: String = ""
) : Serializable
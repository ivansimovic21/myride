package ivan.ba.myride.data.dataModels.dmPhoto

import android.os.Parcel
import android.os.Parcelable

data class DMPhotoExplore(
        var photoID: String = "",
        var carID: String? = null,
        var imagePath: String = "",
        var imageDescription: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(photoID)
        parcel.writeString(imagePath)
        parcel.writeString(imageDescription)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DMPhotoExplore> {
        override fun createFromParcel(parcel: Parcel): DMPhotoExplore {
            return DMPhotoExplore(parcel)
        }

        override fun newArray(size: Int): Array<DMPhotoExplore?> {
            return arrayOfNulls(size)
        }
    }
}
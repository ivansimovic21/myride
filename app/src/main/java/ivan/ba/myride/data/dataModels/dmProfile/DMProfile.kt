package ivan.ba.myride.data.dataModels.dmProfile

data class DMProfile(var id: String, var imagePath: String, var userName: String, var carsCount: Long, var photoCount: Long, var followers: Long, var following: Long)
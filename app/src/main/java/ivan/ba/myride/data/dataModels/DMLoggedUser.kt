package ivan.ba.myride.data.dataModels

data class DMLoggedUser(val name: String,
                        val profilePicturePath: String,
                        val registrationTokens: MutableList<String>,
                        var carsCount: Long,
                        var photoCount: Long,
                        var followers: Long,
                        var following: Long) {
    constructor() : this("",  "", mutableListOf(),0,0,0,0)
}
package ivan.ba.myride.data.fireStoreImpl

import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarInsert
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile

interface FSCarImpl {

    fun getCarsByUserID(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit)
    fun getCarByID(CarID: String, onFinish: (DMCar?) -> Unit)
    fun insertCar(item: DMCarInsert, onFinish: (String?) -> Unit)
    fun updateCar(item: DMCar, onFinish: (Boolean) -> Unit)
    fun deleteCarByID(id: String, onFinish: (Boolean) -> Unit)
    fun updateCarImagePath(carID: String, imagePath: String, onFinish: (Boolean) -> Unit)
}
package ivan.ba.myride.view.car.carDetails

import ivan.ba.myride.data.dataModels.dmCar.DMCar

interface CarDetailsMVP{
    interface View{

        fun showError()
        fun showCar(it: DMCar)
    }
    interface Presenter{
        fun setUpView(view: View)
        fun removeView()
        fun loadCar(carID: String)

    }
    interface Model{

        fun getCar(carID: String, onFinish: (DMCar?) -> Unit)
    }
}
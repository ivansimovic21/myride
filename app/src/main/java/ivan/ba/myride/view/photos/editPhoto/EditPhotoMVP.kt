package ivan.ba.myride.view.photos.editPhoto

import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoInsert
import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto

interface EditPhotoMVP {
    interface View {
        fun finish()
        fun showError()

    }

    interface Presenter {
        fun setUpView(view: View)
        fun setData(car: DMCarProfile?, image: ByteArray)
        fun uploadPhoto(imageDescription: String, isForProfile: Boolean, isUserProfile: Boolean, refId: String?)
        fun removeView()
    }

    interface Model {
        fun insertPhoto(mPhoto: DMPhotoInsert, onFinish: (String?) -> Unit)

        fun insertUserProfilePictures(item: DMProfilePhoto, onFinish: (Boolean) -> Unit)
        fun insertCarProfilePictures(item: DMProfilePhoto, onFinish: (Boolean) -> Unit)
    }
}
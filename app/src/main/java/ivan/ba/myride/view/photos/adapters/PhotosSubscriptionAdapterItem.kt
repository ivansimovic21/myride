package ivan.ba.myride.view.photos.adapters

import android.content.Context
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.firestoreUtil.StorageUtil
import kotlinx.android.synthetic.main.item_photo_subscription.*

class PhotosSubscriptionAdapterItem(val item: DMPhotoExplore,
                               private val context: Context)
    : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        if (item.imagePath != "")
            GlideApp.with(context)
                    .load(StorageUtil.pathToReference(item.imagePath))
                    .placeholder(R.drawable.ic_directions_car_black_24dp)
                    .into(viewHolder.item_photo_subscription_img)
    }

    override fun getLayout() = R.layout.item_photo_subscription
}
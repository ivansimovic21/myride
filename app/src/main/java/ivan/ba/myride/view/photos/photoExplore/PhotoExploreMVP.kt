package ivan.ba.myride.view.photos.photoExplore

import android.support.annotation.UiThread
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore

interface PhotoExploreMVP{
    interface View{

        @UiThread
        fun updateRecyclerView(items: ArrayList<DMPhotoExplore>)

        fun showError()
    }
    interface Presenter{

        fun setUpView(view: View)
        fun removeView()
        fun getPhotos()
    }
    interface Model{

        fun getPhotos(onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
    }
}
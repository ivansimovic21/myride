package ivan.ba.myride.view.car.carPhotosForExplore

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ivan.ba.myride.R
import ivan.ba.myride.view.car.carEdit.CarEditMVP
import javax.inject.Inject

class CarPhotosForExploreFragment : Fragment(), CarPhotosForExploreMVP.View {
    private var listener: OnFragmentInteractionListener? = null

    @Inject
    lateinit var presenter: CarPhotosForExploreMVP.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_car_photos_for_explore, container, false)
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement InteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

}

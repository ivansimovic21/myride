package ivan.ba.myride.view.car.carEdit

class CarEditPresenter(val model: CarEditMVP.Model) : CarEditMVP.Presenter {
    var mCarID = ""

    var view: CarEditMVP.View? = null
    override fun setUpView(view: CarEditMVP.View) {
        this.view = view
    }

    override fun save(carName: String) {
        if (mCarID == "")
            insertCar(carName)
        else
            updateCar(carName)
    }

    override fun insertCar(carName: String) {
        model.insertCar(carName) {
            if (it != null) {
                view?.success()
                mCarID = it
            } else
                view?.showError()
        }
    }

    override fun updateCar(carName: String) {
        model.updateCar(mCarID, carName) {
            if (it) {
                view?.success()
            } else
                view?.showError()
        }
    }

    override fun removeView() {
        view = null
    }

    override fun loadCarForUpdate(carID: String) {
        mCarID = carID
        model.getCar(carID) {
            if (it != null)
                view?.showCar(it)
            else
                view?.showError()
        }
    }

    override fun selectImageForCarPhoto() {
        view?.selectImageForCarPhoto(mCarID)
    }

}
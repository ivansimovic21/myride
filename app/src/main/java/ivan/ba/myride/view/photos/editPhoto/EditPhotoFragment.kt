package ivan.ba.myride.view.photos.editPhoto

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.view.photos.takePhoto.TakePhotoFragmentArgs
import kotlinx.android.synthetic.main.fragment_edit_photo.view.*
import org.jetbrains.anko.design.longSnackbar
import java.io.ByteArrayOutputStream
import javax.inject.Inject


class EditPhotoFragment : Fragment(), EditPhotoMVP.View {

    @Inject
    lateinit var presenter: EditPhotoMVP.Presenter

    lateinit var viewContainer: RelativeLayout
    lateinit var mNav: NavController

    var mIsForProfile = false
    var mIsUserProfile = false
    var refId: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_edit_photo, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        mNav = findNavController()
        val mImageBitmap = EditPhotoFragmentArgs.fromBundle(arguments).selectedImage

        val outputStream = ByteArrayOutputStream()

        mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)
        val mSelectedImageBytes = outputStream.toByteArray()

        val car = EditPhotoFragmentArgs.fromBundle(arguments).car
        mIsForProfile = EditPhotoFragmentArgs.fromBundle(arguments).isForProfile
        mIsUserProfile = EditPhotoFragmentArgs.fromBundle(arguments).isUserProfile
        refId = EditPhotoFragmentArgs.fromBundle(arguments).refId

        presenter.setData(car, mSelectedImageBytes)

        viewContainer = view.edit_photo_rl
        view.edit_photo_img.setImageBitmap(mImageBitmap)
        view.edit_photo_btn_back.setOnClickListener {
            mNav.navigateUp()
        }
        view.edit_photo_btn_next.setOnClickListener {
            if (mIsForProfile && !mIsUserProfile)
                presenter.uploadPhoto(view.edit_photo_txt_description.text.toString(), mIsForProfile, mIsUserProfile, refId)
        }
        return view
    }

    override fun finish() {
        if (mIsForProfile && mIsUserProfile)
            mNav.popBackStack(R.id.myProfileFragment, true)
        else if (mIsForProfile && !mIsUserProfile)
            mNav.popBackStack(R.id.carEditFragment, false)
        else
            mNav.popBackStack(R.id.carsForPhotoFragment, true)

    }

    override fun showError() {
        longSnackbar(viewContainer, "Network error")
    }

    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }

}

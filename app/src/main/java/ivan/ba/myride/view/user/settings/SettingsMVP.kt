package ivan.ba.myride.view.user.settings

interface SettingsMVP {
    interface View{

    }
    interface Presenter{

    }
    interface Model{

    }
}
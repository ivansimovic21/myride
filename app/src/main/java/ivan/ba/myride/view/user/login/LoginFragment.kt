package ivan.ba.myride.view.user.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.iid.FirebaseInstanceId
import ivan.ba.myride.R
import ivan.ba.myride.firestoreUtil.FirestoreUtil
import ivan.ba.myride.service.MyFirebaseInstanceIDService
import ivan.ba.myride.util.options.MyOptions
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.support.v4.indeterminateProgressDialog

class LoginFragment : Fragment() {

    private val RC_SIGN_IN = 1

    private val signInProviders = listOf(AuthUI.IdpConfig.EmailBuilder().setAllowNewAccounts(true).setRequireName(true).build())

    lateinit var mNav: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        view.account_sign_in.setOnClickListener {
            val intent = AuthUI.getInstance().createSignInIntentBuilder()
                    .setAvailableProviders(signInProviders)
                    .setLogo(R.drawable.fui_ic_mail_white_24dp)
                    .build()
            startActivityForResult(intent, RC_SIGN_IN)
        }
        mNav = findNavController()
        return view
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == Activity.RESULT_OK) {
                val progressDialog = indeterminateProgressDialog("Setting up your account")
                FirestoreUtil.initCurrentUserIfFirstTime {

                    val registrationToken = FirebaseInstanceId.getInstance().token
                    MyFirebaseInstanceIDService.addTokenToFirestore(registrationToken)

                    FirestoreUtil.getCurrentUser {
                        MyOptions.loggedUser.SET(it)
                        progressDialog.dismiss()
                        mNav.navigate(R.id.photoExploreFragment)
                    }


                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                if (response == null)
                    return
                when (response.error?.errorCode) {
                    ErrorCodes.NO_NETWORK -> longSnackbar(constraint_layout, "No network")
                    ErrorCodes.UNKNOWN_ERROR -> longSnackbar(constraint_layout, "Unknown error")
                }
            }
        }
    }

}

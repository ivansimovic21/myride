package ivan.ba.myride.view.profile.profilePictures

import android.support.annotation.UiThread
import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto

interface ProfilePhotoMVP {
    interface View {
        @UiThread
        fun showError()

        @UiThread
        fun updateRecyclerView(items: ArrayList<DMProfilePhoto>)
    }

    interface Presenter {
        fun removeView()
        fun setUpView(view: View)
        fun loadData(refId: String, isUser: Boolean)

    }

    interface Model {
        fun getUserProfilePhotos(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit)
        fun insertUserProfilePhotos(item: DMProfilePhoto, onFinish: (Boolean) -> Unit)
        fun getCarProfilePhotos(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit)
        fun insertCarProfilePhotos(item: DMProfilePhoto, onFinish: (Boolean) -> Unit)
    }
}
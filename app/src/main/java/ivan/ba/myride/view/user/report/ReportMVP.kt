package ivan.ba.myride.view.user.report

interface ReportMVP{
    interface View{

    }
    interface Presenter{

    }
    interface Model{

    }
}
package ivan.ba.myride.view.car.carsOfUser

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.OnItemClickListener
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.view.car.adapters.CarsOfUserAdapterItem
import kotlinx.android.synthetic.main.fragment_cars_of_user.*
import kotlinx.android.synthetic.main.fragment_cars_of_user.view.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class CarsOfUserFragment : Fragment(), CarsOfUserMVP.View {

    private var shouldInitRecyclerView = true
    private lateinit var section: Section

    var isMyProfileCars: Boolean = false

    @Inject
    lateinit var presenter: CarsOfUserMVP.Presenter
    lateinit var mNav: NavController
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_cars_of_user, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        presenter.setUserID(CarsOfUserFragmentArgs.fromBundle(arguments).userId)
        isMyProfileCars = CarsOfUserFragmentArgs.fromBundle(arguments).isMyProfileCars
        if(!isMyProfileCars)
            view.cars_of_user_btn_add.visibility = GONE
        mNav = findNavController()
        view.cars_of_user_btn_add.setOnClickListener {
            val action = CarsOfUserFragmentDirections.actionCarsOfUserFragmentToCarEditFragment("")
            mNav.navigate(action)
        }
        view.cars_of_user_btn_back.setOnClickListener {
            mNav.popBackStack()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        shouldInitRecyclerView = true
        presenter.getCarsOfUser()

    }

    @UiThread
    override fun showError() {
        longSnackbar(cars_of_users_rl, "No network")
    }

    @UiThread
    override fun updateRecyclerView(items: ArrayList<DMCarProfile>) {

        val listOfCarsAdapter = ArrayList<CarsOfUserAdapterItem>()
        for (i in items) {
            listOfCarsAdapter.add(CarsOfUserAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerView) {
            cars_of_user_rv.apply {
                layoutManager = LinearLayoutManager(this@CarsOfUserFragment.context)
                adapter = GroupAdapter<ViewHolder>().apply {
                    section = Section(listOfCarsAdapter)
                    add(section)
                    setOnItemClickListener(onItemClick)
                }
            }
            shouldInitRecyclerView = false
        } else
            section.update(listOfCarsAdapter)

    }

    private val onItemClick = OnItemClickListener { item, view ->
        if (item is CarsOfUserAdapterItem) {
            if(isMyProfileCars) {
                val action = CarsOfUserFragmentDirections.actionCarsOfUserFragmentToCarEditFragment(item.car.carID)
                mNav.navigate(action)
            }else{
                val action = CarsOfUserFragmentDirections.actionCarsOfUserFragmentToCarDetailsFragment(item.car.carID)
                mNav.navigate(action)
            }
        }
    }
}

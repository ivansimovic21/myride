package ivan.ba.myride.view.photos.photoDetailsEdit


import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.firestoreUtil.StorageUtil
import kotlinx.android.synthetic.main.fragment_photo_details.*
import kotlinx.android.synthetic.main.fragment_photo_details.view.*
import kotlinx.android.synthetic.main.fragment_photo_explore.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class PhotoDetailsEditFragment : Fragment(), PhotoDetailsEditMVP.View {
    override fun loadCarPhoto(carImagePath: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadUserPhoto(userImagePath: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openUserProfile(userID: String, nothing: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    @Inject
    lateinit var presenter: PhotoDetailsEditMVP.Presenter
    lateinit var mNav : NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_photo_details, container, false)
        MyApp.graph.inject(this)
        mNav = findNavController()
        presenter.setUpView(this)
        val photoID = PhotoDetailsEditFragmentArgs.fromBundle(arguments).photoId
        presenter.setUpData(photoID)
        presenter.loadPhoto()

        view.photo_details_txt_comments.setOnClickListener {
            val action = PhotoDetailsEditFragmentDirections.actionPhotoDetailsEditFragmentToCommentsFragment(photoID)
            mNav.navigate(action)
        }
        view.photo_details_btn_back.setOnClickListener {
            mNav.popBackStack()
        }

        return view
    }

    override fun setUpView(photo: DMPhoto) {
        photo_details_txt_car.text = photo.carName
        photo_details_txt_user.text = photo.userName
        photo_details_txt_likes.text = photo.likes.toString()
        photo_details_txt_comments.text = photo.comments.toString()
        photo_details_txt_description.text = photo.imageDescription
    }

    override fun loadPhoto(imagePath: String) {
        GlideApp.with(this)
                .load(StorageUtil.pathToReference(imagePath))
                .placeholder(R.drawable.ic_directions_car_black_24dp)
                .into(photo_details_img_photo)
    }


    @UiThread
    override fun showError() {
        longSnackbar(photo_explore_rl, "Network error")
    }


    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }

}

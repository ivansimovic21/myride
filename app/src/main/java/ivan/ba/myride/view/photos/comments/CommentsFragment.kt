package ivan.ba.myride.view.photos.comments

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment
import ivan.ba.myride.view.photos.adapters.PhotoCommentsAdapterItem
import ivan.ba.myride.view.photos.photoDetails.PhotoDetailsFragmentArgs
import kotlinx.android.synthetic.main.fragment_comments.*
import kotlinx.android.synthetic.main.fragment_comments.view.*
import javax.inject.Inject


class CommentsFragment : Fragment(), CommentsMVP.View {

    companion object {
        const val ARG_PHOTO_ID = "ARG_PHOTO_ID"
    }


    @Inject
    lateinit var presenter: CommentsMVP.Presenter

    private var shouldInitRecyclerViewCars = true
    private lateinit var sectionComments: Section

    lateinit var mNav: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_comments, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        val photoID = CommentsFragmentArgs.fromBundle(arguments).photoId
        presenter.loadComments(photoID)
        mNav = findNavController()

        view.comments_btn_send.setOnClickListener {
            presenter.inserComment(comments_text_WriteComment.text.toString())
            comments_text_WriteComment.text.clear()
        }

        view.comments_btn_back.setOnClickListener {
            mNav.popBackStack()
        }

        return view
    }


    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }

    @UiThread
    override fun updateComments(items: ArrayList<DMComment>) {
        val listOfAdapter = ArrayList<PhotoCommentsAdapterItem>()
        for (i in items) {
            listOfAdapter.add(PhotoCommentsAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerViewCars) {
            comments_rw.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = GroupAdapter<ViewHolder>().apply {
                    sectionComments = Section(listOfAdapter)
                    add(sectionComments)
                }
            }
            shouldInitRecyclerViewCars = false
        } else
            sectionComments.update(listOfAdapter)

    }

    @UiThread
    override fun showError() {

    }


}

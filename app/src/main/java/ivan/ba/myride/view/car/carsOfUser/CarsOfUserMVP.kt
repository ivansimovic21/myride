package ivan.ba.myride.view.car.carsOfUser

import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile

interface CarsOfUserMVP {
    interface View {
        fun showError()
        fun updateRecyclerView(items: ArrayList<DMCarProfile>)
    }

    interface Presenter {
        fun getCarsOfUser()
        fun setUpView(view: View)
        fun setUserID(userID: String)
        fun removeView()
    }

    interface Model {
        fun getCarsOfUser(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit)
    }
}
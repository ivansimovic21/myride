package ivan.ba.myride.view.profile.adapters

import android.content.Context
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.firestoreUtil.StorageUtil
import kotlinx.android.synthetic.main.item_cars_profile.*

class CarsProfileAdapterItem(val car: DMCarProfile,
                             private val context: Context)
    : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.item_cars_profile_lbl.text = car.name
        if (car.imagePath != "")
            GlideApp.with(context)
                    .load(StorageUtil.pathToReference(car.imagePath))
                    .placeholder(R.drawable.ic_directions_car_black_24dp)
                    .into(viewHolder.item_cars_profile_img)
    }

    override fun getLayout() = R.layout.item_cars_profile
}
package ivan.ba.myride.view.photos.takePhoto

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import kotlinx.android.synthetic.main.fragment_take_photo.view.*
import java.io.ByteArrayOutputStream
import java.io.File


class TakePhotoFragment : Fragment() {

    private val RC_SELECT_IMAGE = 2
    private val TAKE_PICTURE = 1

    private var imageUri: Uri? = null

    lateinit var mNav: NavController
    var mCar: DMCarProfile? = null

    var mIsForProfile = false
    var mIsUserProfile = false
    var refId : String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_take_photo, container, false)

        mNav = findNavController()
        mCar = TakePhotoFragmentArgs.fromBundle(arguments).car
        mIsForProfile = TakePhotoFragmentArgs.fromBundle(arguments).isForProfile
        mIsUserProfile = TakePhotoFragmentArgs.fromBundle(arguments).isUserProfile
        refId = TakePhotoFragmentArgs.fromBundle(arguments).refId

        ActivityCompat.requestPermissions(this.activity!!,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                1);

        view.takePhoto_img_folder.setOnClickListener {
            val intent = Intent().apply {
                type = "image/*"
                action = Intent.ACTION_GET_CONTENT
                putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/jpeg", "image/png"))
            }
            startActivityForResult(Intent.createChooser(intent, "Select Image"), RC_SELECT_IMAGE)
        }
        view.takePhoto_img_photo.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val photo = File(Environment.getExternalStorageDirectory(), "Pic.jpg")
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo))
            imageUri = Uri.fromFile(photo)
            startActivityForResult(intent, TAKE_PICTURE)
        }
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
/*            when (requestCode) {
                RC_SELECT_IMAGE -> {*/
            var selectedImagePath: Uri? = null
            if (requestCode == TAKE_PICTURE)
                selectedImagePath = imageUri
            else if (data != null && data.data != null)
                selectedImagePath = data.data

            val selectedImageBmp = MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedImagePath)
            val resizedBitmap = Bitmap.createScaledBitmap(selectedImageBmp, 640, 640, false)

            val action = TakePhotoFragmentDirections.actionTakePhotoFragmentToEditPhotoFragment(resizedBitmap, mCar,mIsForProfile,mIsUserProfile,refId)
            mNav.navigate(action)

            /*  }
              TAKE_PICTURE -> {
                  val selectedImage = imageUri
                  context?.contentResolver?.notifyChange(selectedImage, null)
                  val bitmap: Bitmap
                  try {
                      bitmap = android.provider.MediaStore.Images.Media
                              .getBitmap(cr, selectedImage)

                      imageView.setImageBitmap(bitmap)
                      Toast.makeText(this, selectedImage.toString(),
                              Toast.LENGTH_LONG).show()
                  } catch (e: Exception) {
                      Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
                              .show()
                      Log.e("Camera", e.toString())
                  }

              }
          }*/
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    mNav.navigateUp()
                }
                return
            }
        }
    }

}

package ivan.ba.myride.view.photos.selectCarForPhoto

import android.support.annotation.UiThread
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.view.car.carsOfUser.CarsOfUserMVP

interface SelectCarForPhotoMVP{
    interface View{

        @UiThread
        fun showError()

        @UiThread
        fun updateRecyclerView(items: ArrayList<DMCarProfile>)
    }
    interface Presenter{

        fun setUpView(view: SelectCarForPhotoMVP.View)
        fun removeView()
        fun getCarsOfUser()
    }
    interface Model{

        fun getCarsOfUser(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit)
    }
}
package ivan.ba.myride.view.profile.otherProfile

import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore


class OtherProfilePresenter(val model: OtherProfileMVP.Model) : OtherProfileMVP.Presenter {

    lateinit var mPhotos: ArrayList<DMPhotoExplore>
    var view: OtherProfileMVP.View? = null
    override fun setUpView(view: OtherProfileMVP.View) {
        this.view = view
    }

    override fun removeView() {
        this.view = null
    }

    override fun getAllPhotos(userID: String, carID: String?) {
        model.getPhotosByUserID(userID) {
            if (it != null) {
                mPhotos = it
                getCarsByUserID(userID, carID)
                view?.updatePhotosRecyclerView(it)
            } else
                view?.showError()
        }
    }

    override fun showAllPhotos() {
        view?.updatePhotosRecyclerView(mPhotos)
    }

    override fun showPhotosByCarID(carID: String?) {
        view?.updatePhotosRecyclerView(mPhotos.filter { x -> x.carID == carID } as ArrayList<DMPhotoExplore>)
    }

    override fun showSelectedPhotos(carID: String) {
        if (carID.equals("all"))
            showAllPhotos()
        else {
            val mCarID: String?
            if (carID.equals(""))
                mCarID = null
            else
                mCarID = carID
            showPhotosByCarID(mCarID)
        }
    }

    override fun getCarsByUserID(userID: String, carID: String?) {
        model.getCarsByUserID(userID) {
            if (it != null) {
                val countPhotosWithoutCar = mPhotos.filter { x -> x.carID == null }.size
                if (countPhotosWithoutCar != 0 && countPhotosWithoutCar != mPhotos.size) {
                    it.add(0, DMCarProfile("", "", "No car", ""))
                }
                it.add(0, DMCarProfile("all", "", "All", ""))

                view?.updateCarsRecyclerView(it)
                if (carID != null)
                    view?.selectCar(it.indexOf(it.filter { x -> x.carID == carID }.first()))
            } else
                view?.showError()
        }
    }
}

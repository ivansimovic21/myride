package ivan.ba.myride.view.photos.comments

import com.google.firebase.auth.FirebaseAuth
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment
import ivan.ba.myride.util.options.MyOptions

class CommentsPresenter(val model: CommentsMVP.Model) : CommentsMVP.Presenter {
    var mListOfComments = ArrayList<DMComment>()
    var mPhotoID = ""
    var view: CommentsMVP.View? = null
    override fun setUpView(view: CommentsMVP.View) {
        this.view = view
    }

    override fun removeView() {
        this.view = null
    }

    override fun loadComments(photoID: String) {
        mPhotoID = photoID
        model.getCommentsByPhotoID(photoID) {
            if (it != null) {
                mListOfComments.clear()
                mListOfComments.addAll(it)
                view?.updateComments(mListOfComments)
            } else {
                view?.showError()
            }
        }
    }

    override fun inserComment(commentText: String) {
        val comment = DMComment("", mPhotoID, FirebaseAuth.getInstance().currentUser!!.uid, MyOptions.loggedUser.GET()!!.name, MyOptions.loggedUser.GET()!!.profilePicturePath, commentText)
        model.insertComment(comment) {
            if (it != null) {
                comment.commentID = it
                mListOfComments.add(comment)
                view?.updateComments(mListOfComments)
            }
        }
    }
}
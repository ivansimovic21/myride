package ivan.ba.myride.view.photos.photoExplore

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl

class PhotoExploreModel(val repPhoto: RepPhotoImpl)  : PhotoExploreMVP.Model{

    override fun getPhotos(onFinish: (ArrayList<DMPhotoExplore>?) -> Unit){
        repPhoto.getPhotosForExplore(onFinish)
    }

}
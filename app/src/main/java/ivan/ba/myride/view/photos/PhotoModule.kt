package ivan.ba.myride.view.photos

import dagger.Module
import dagger.Provides
import ivan.ba.myride.data.reposetory.RepPhoto
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl
import ivan.ba.myride.data.reposetoryImpl.RepProfilePhotoImpl
import ivan.ba.myride.view.photos.bookmarks.BookmarksMVP
import ivan.ba.myride.view.photos.bookmarks.BookmarksModel
import ivan.ba.myride.view.photos.bookmarks.BookmarksPresenter
import ivan.ba.myride.view.photos.comments.CommentsMVP
import ivan.ba.myride.view.photos.comments.CommentsModel
import ivan.ba.myride.view.photos.comments.CommentsPresenter
import ivan.ba.myride.view.photos.editPhoto.EditPhotoMVP
import ivan.ba.myride.view.photos.editPhoto.EditPhotoModel
import ivan.ba.myride.view.photos.editPhoto.EditPhotoPresenter
import ivan.ba.myride.view.photos.photoDetails.PhotoDetailsMVP
import ivan.ba.myride.view.photos.photoDetails.PhotoDetailsModel
import ivan.ba.myride.view.photos.photoDetails.PhotoDetailsPresenter
import ivan.ba.myride.view.photos.photoDetailsEdit.PhotoDetailsEditMVP
import ivan.ba.myride.view.photos.photoDetailsEdit.PhotoDetailsEditModel
import ivan.ba.myride.view.photos.photoDetailsEdit.PhotoDetailsEditPresenter
import ivan.ba.myride.view.photos.photoEdit.PhotoEditMVP
import ivan.ba.myride.view.photos.photoEdit.PhotoEditModel
import ivan.ba.myride.view.photos.photoEdit.PhotoEditPresenter
import ivan.ba.myride.view.photos.photoExplore.PhotoExploreMVP
import ivan.ba.myride.view.photos.photoExplore.PhotoExploreModel
import ivan.ba.myride.view.photos.photoExplore.PhotoExplorePresenter
import ivan.ba.myride.view.photos.photoSubscription.PhotoSubscriptionMVP
import ivan.ba.myride.view.photos.photoSubscription.PhotoSubscriptionModel
import ivan.ba.myride.view.photos.photoSubscription.PhotoSubscriptionPresenter
import ivan.ba.myride.view.photos.selectCarForPhoto.SelectCarForPhotoMVP
import ivan.ba.myride.view.photos.selectCarForPhoto.SelectCarForPhotoModel
import ivan.ba.myride.view.photos.selectCarForPhoto.SelectCarForPhotoPresenter
import javax.inject.Singleton

@Module
class PhotoModule {
    @Provides
    fun providePhotoDetailsPresenter(model: PhotoDetailsMVP.Model): PhotoDetailsMVP.Presenter {
        return PhotoDetailsPresenter(model)
    }

    @Provides
    @Singleton
    fun providePhotoDetailsModel(repPhoto: RepPhotoImpl): PhotoDetailsMVP.Model {
        return PhotoDetailsModel(repPhoto)
    }

    @Provides
    fun providePhotoDetailsEditPresenter(model: PhotoDetailsEditMVP.Model): PhotoDetailsEditMVP.Presenter {
        return PhotoDetailsEditPresenter(model)
    }

    @Provides
    @Singleton
    fun providePhotoDetailsEditModel(repPhoto: RepPhotoImpl): PhotoDetailsEditMVP.Model {
        return PhotoDetailsEditModel(repPhoto)
    }

    @Provides
    fun providePhotoEditPresenter(model: PhotoEditMVP.Model): PhotoEditMVP.Presenter {
        return PhotoEditPresenter(model)
    }

    @Provides
    @Singleton
    fun providePhotoEditModel(repPhoto: RepPhotoImpl): PhotoEditMVP.Model {
        return PhotoEditModel(repPhoto)
    }

    @Provides
    fun providePhotoExplorePresenter(model: PhotoExploreMVP.Model): PhotoExploreMVP.Presenter {
        return PhotoExplorePresenter(model)
    }

    @Provides
    @Singleton
    fun providePhotoExploreModel(repPhoto: RepPhotoImpl): PhotoExploreMVP.Model {
        return PhotoExploreModel(repPhoto)
    }

    @Provides
    fun providePhotoSubscriptionPresenter(model: PhotoSubscriptionMVP.Model): PhotoSubscriptionMVP.Presenter {
        return PhotoSubscriptionPresenter(model)
    }

    @Provides
    @Singleton
    fun providePhotoSubscriptionModel(repPhoto: RepPhotoImpl): PhotoSubscriptionMVP.Model {
        return PhotoSubscriptionModel(repPhoto)
    }

    @Provides
    fun provideBookmarksPresenter(model: BookmarksMVP.Model): BookmarksMVP.Presenter {
        return BookmarksPresenter(model)
    }

    @Provides
    @Singleton
    fun provideBookmarksModel(repPhoto: RepPhotoImpl): BookmarksMVP.Model {
        return BookmarksModel(repPhoto)
    }

    @Provides
    fun provideSelectCarForPhotoPresenter(model: SelectCarForPhotoMVP.Model): SelectCarForPhotoMVP.Presenter {
        return SelectCarForPhotoPresenter(model)
    }


    @Provides
    @Singleton
    fun provideSelectCarForPhotoModel(repCar: RepCarImpl): SelectCarForPhotoMVP.Model {
        return SelectCarForPhotoModel(repCar)
    }

    @Provides
    @Singleton
    fun provideEditPhotoModel(repPhoto: RepPhotoImpl, repProfilePhoto: RepProfilePhotoImpl): EditPhotoMVP.Model {
        return EditPhotoModel(repPhoto, repProfilePhoto)
    }

    @Provides
    fun provideEditPhotoPresenter(model: EditPhotoMVP.Model): EditPhotoMVP.Presenter {
        return EditPhotoPresenter(model)
    }

    @Provides
    @Singleton
    fun provideCommentsModel(repPhoto: RepPhotoImpl): CommentsMVP.Model {
        return CommentsModel(repPhoto)
    }

    @Provides
    fun provideCommentsPresenter(model: CommentsMVP.Model): CommentsMVP.Presenter {
        return CommentsPresenter(model)
    }

    @Provides
    @Singleton
    fun providePhotoRepository(): RepPhotoImpl {
        return RepPhoto()
    }
}
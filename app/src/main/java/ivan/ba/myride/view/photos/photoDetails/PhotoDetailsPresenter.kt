package ivan.ba.myride.view.photos.photoDetails

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto

class PhotoDetailsPresenter(val model: PhotoDetailsMVP.Model) : PhotoDetailsMVP.Presenter {

    var photo: DMPhoto? = null

    override fun handleUserClick() {
        if (photo != null)
            view?.openUserProfile(photo!!.userID, null)
    }

    override fun handleCarClick() {
        if (photo != null)
            view?.openUserProfile(photo!!.userID, photo!!.carID)
    }

    var view: PhotoDetailsMVP.View? = null
    override fun setUpView(view: PhotoDetailsMVP.View) {
        this.view = view
    }

    var mPhotoID: String = ""

    override fun setUpData(photoID: String) {
        mPhotoID = photoID
    }

    override fun removeView() {
        this.view = null
    }

    override fun loadPhoto() {
        model.loadPhoto(mPhotoID) {
            if (it != null) {
                photo = it
                view?.setUpView(it)
                if (it.imagePath != "")
                    view?.loadPhoto(it.imagePath)
                if (it.carImagePath != null && it.carImagePath != "")
                    view?.loadCarPhoto(it.carImagePath!!)
                if (it.userImagePath != "")
                    view?.loadUserPhoto(it.userImagePath)
            } else {
                view?.showError()
            }
        }
    }
}
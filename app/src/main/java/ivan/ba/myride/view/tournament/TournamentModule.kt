package ivan.ba.myride.view.tournament

import dagger.Module
import dagger.Provides
import ivan.ba.myride.data.reposetory.RepTournament
import ivan.ba.myride.data.reposetoryImpl.RepTournamentImpl
import ivan.ba.myride.view.tournament.tournamentAddPhoto.TournamentAddPhotoMVP
import ivan.ba.myride.view.tournament.tournamentAddPhoto.TournamentAddPhotoModel
import ivan.ba.myride.view.tournament.tournamentAddPhoto.TournamentAddPhotoPresenter
import ivan.ba.myride.view.tournament.tournamentPhotoList.TournamentPhotoListMVP
import ivan.ba.myride.view.tournament.tournamentPhotoList.TournamentPhotoListModel
import ivan.ba.myride.view.tournament.tournamentPhotoList.TournamentPhotoListPresenter
import ivan.ba.myride.view.tournament.tournamentVictoryScreen.TournamentVictoryScreenMVP
import ivan.ba.myride.view.tournament.tournamentVictoryScreen.TournamentVictoryScreenModel
import ivan.ba.myride.view.tournament.tournamentVictoryScreen.TournamentVictoryScreenPresenter
import javax.inject.Singleton

@Module
class TournamentModule {
    @Provides
    fun provideTournamentPhotoListPresenter(model: TournamentPhotoListMVP.Model): TournamentPhotoListMVP.Presenter {
        return TournamentPhotoListPresenter(model)
    }

    @Provides
    @Singleton
    fun provideTournamentPhotoListModel(rep: RepTournamentImpl): TournamentPhotoListMVP.Model {
        return TournamentPhotoListModel(rep)
    }

    @Provides
    @Singleton
    fun provideTournamentAddPhotoModel(rep: RepTournamentImpl): TournamentAddPhotoMVP.Model {
        return TournamentAddPhotoModel(rep)
    }

    @Provides
    fun provideTournamentAddPhotoPresenter(model: TournamentAddPhotoMVP.Model): TournamentAddPhotoMVP.Presenter {
        return TournamentAddPhotoPresenter(model)
    }

    @Provides
    @Singleton
    fun provideTournamentVictoryScreenModel(rep: RepTournamentImpl): TournamentVictoryScreenMVP.Model {
        return TournamentVictoryScreenModel(rep)
    }

    @Provides
    fun provideTournamentVictoryScreenPresenter(model: TournamentVictoryScreenMVP.Model): TournamentVictoryScreenMVP.Presenter {
        return TournamentVictoryScreenPresenter(model)
    }

    @Provides
    @Singleton
    fun provideTournamentRepository(): RepTournamentImpl {
        return RepTournament()
    }
}

package ivan.ba.myride.view.photos.photoSubscription

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ivan.ba.myride.R
import javax.inject.Inject

class PhotoSubscriptionFragment : Fragment(), PhotoSubscriptionMVP.View {
    private var listener: InteractionListener? = null

    @Inject
    lateinit var presenter: PhotoSubscriptionMVP.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photo_subscription, container, false)
    }

    fun onButtonPressed(uri: Uri) {
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is InteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement InteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface InteractionListener {
    }

}

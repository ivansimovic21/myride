package ivan.ba.myride.view.car.carDetails

import ivan.ba.myride.view.car.carEdit.CarEditMVP

class CarDetailsPresenter(val model: CarDetailsMVP.Model) : CarDetailsMVP.Presenter {


    var view: CarDetailsMVP.View? = null

    override fun setUpView(view: CarDetailsMVP.View){
        this.view = view
    }

    override fun removeView(){
        view = null
    }

    override fun loadCar(carID: String) {
        model.getCar(carID){
            if(it != null)
                view?.showCar(it)
            else
                view?.showError()
        }
    }
}
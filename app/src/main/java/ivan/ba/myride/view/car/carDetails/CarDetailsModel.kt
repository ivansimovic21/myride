package ivan.ba.myride.view.car.carDetails

import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl

class CarDetailsModel(val repCar: RepCarImpl):CarDetailsMVP.Model{

    override fun getCar(carID: String, onFinish: (DMCar?) -> Unit) {
        repCar.getCarByID(carID,onFinish)
    }

}
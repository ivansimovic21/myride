package ivan.ba.myride.view.car.carsOfUser

import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl

class CarsOfUserModel(val repCar: RepCarImpl) : CarsOfUserMVP.Model{
    override fun getCarsOfUser(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit) {
        repCar.getCarsByUserID(userID,onFinish)
    }

}
package ivan.ba.myride.view.photos.photoExplore

class PhotoExplorePresenter(val model: PhotoExploreMVP.Model) : PhotoExploreMVP.Presenter {
    var view: PhotoExploreMVP.View? = null
    override fun setUpView(view: PhotoExploreMVP.View) {
        this.view = view
    }

    override fun removeView() {
        this.view = null
    }

    override fun getPhotos() {
        model.getPhotos {
            if (it != null)
                view?.updateRecyclerView(it)
            else
                view?.showError()
        }
    }
}
package ivan.ba.myride.view.car.carsOnExplore

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ivan.ba.myride.R
import ivan.ba.myride.view.car.carsOfUser.CarsOfUserMVP
import javax.inject.Inject

class CarsOnExploreFragment : Fragment(), CarsOnExploreMVP.View {
    private var listener: OnFragmentInteractionListener? = null

    @Inject
    lateinit var presenter: CarsOnExploreMVP.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cars_on_explore, container, false)
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
/*        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement InteractionListener")
        }*/
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

}

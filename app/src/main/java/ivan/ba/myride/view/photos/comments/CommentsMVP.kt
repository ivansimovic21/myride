package ivan.ba.myride.view.photos.comments

import android.support.annotation.UiThread
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment

interface CommentsMVP {
    interface View {

        @UiThread
        fun updateComments(items: ArrayList<DMComment>)

        @UiThread
        fun showError()
    }

    interface Presenter {

        fun setUpView(view: View)
        fun removeView()
        fun loadComments(photoID: String)
        fun inserComment(commentText: String)
    }

    interface Model {
        fun getCommentsByPhotoID(photoID: String, onFinish: (ArrayList<DMComment>?) -> Unit)
        fun insertComment(item: DMComment, onFinish: (String?) -> Unit)
        fun updateComment(item: DMComment, onFinish: (Boolean) -> Unit)
        fun deleteCommentByID(photoID: String, commentID: String, onFinish: (Boolean) -> Unit)
    }
}
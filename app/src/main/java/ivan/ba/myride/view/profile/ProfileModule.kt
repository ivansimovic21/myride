package ivan.ba.myride.view.profile

import dagger.Module
import dagger.Provides
import ivan.ba.myride.data.reposetory.RepProfile
import ivan.ba.myride.data.reposetory.RepProfilePhoto
import ivan.ba.myride.data.reposetory.RepUser
import ivan.ba.myride.data.reposetoryImpl.*
import ivan.ba.myride.view.profile.myProfile.MyProfileMVP
import ivan.ba.myride.view.profile.myProfile.MyProfileModel
import ivan.ba.myride.view.profile.myProfile.MyProfilePresenter
import ivan.ba.myride.view.profile.otherProfile.OtherProfileMVP
import ivan.ba.myride.view.profile.otherProfile.OtherProfileModel
import ivan.ba.myride.view.profile.otherProfile.OtherProfilePresenter
import ivan.ba.myride.view.profile.profilePictures.ProfilePhotoMVP
import ivan.ba.myride.view.profile.profilePictures.ProfilePhotoModel
import ivan.ba.myride.view.profile.profilePictures.ProfilePhotoPresenter
import javax.inject.Singleton

@Module
class ProfileModule {
    @Provides
    fun provideMyProfilePresenter(model: MyProfileMVP.Model): MyProfileMVP.Presenter {
        return MyProfilePresenter(model)
    }

    @Provides
    @Singleton
    fun provideMyProfileModel(repProfile: RepProfileImpl, repCar: RepCarImpl, repPhoto: RepPhotoImpl): MyProfileMVP.Model {
        return MyProfileModel(repProfile, repCar, repPhoto)
    }

    @Provides
    @Singleton
    fun provideOtherProfileModel(repUser: RepUserImpl, repCar: RepCarImpl, repPhoto: RepPhotoImpl): OtherProfileMVP.Model {
        return OtherProfileModel(repUser, repCar,repPhoto)
    }

    @Provides
    fun provideOtherProfilePresenter(model: OtherProfileMVP.Model): OtherProfileMVP.Presenter {
        return OtherProfilePresenter(model)
    }

    @Provides
    @Singleton
    fun provideProfilePhotoModel(repProfilePhoto: RepProfilePhotoImpl): ProfilePhotoMVP.Model {
        return ProfilePhotoModel(repProfilePhoto)
    }

    @Provides
    fun provideProfilePhotoPresenter(model: ProfilePhotoMVP.Model): ProfilePhotoMVP.Presenter {
        return ProfilePhotoPresenter(model)
    }

    @Provides
    @Singleton
    fun provideUserRepository(): RepUserImpl {
        return RepUser()
    }

    @Provides
    @Singleton
    fun provideProfilePhotoReposetory(): RepProfilePhotoImpl {
        return RepProfilePhoto()
    }
    @Provides
    @Singleton
    fun provideProfileReposetory(): RepProfileImpl {
        return RepProfile()
    }
}
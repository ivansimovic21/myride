package ivan.ba.myride.view.photos.adapters

import android.content.Context
import androidx.navigation.findNavController
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment
import ivan.ba.myride.firestoreUtil.StorageUtil
import ivan.ba.myride.view.photos.comments.CommentsFragmentDirections
import kotlinx.android.synthetic.main.item_photo_comment.*

class PhotoCommentsAdapterItem(val item: DMComment,
                               private val context: Context)
    : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        if (item.userImagePath != "")
            GlideApp.with(context)
                    .load(StorageUtil.pathToReference(item.userImagePath))
                    .placeholder(R.drawable.ic_directions_car_black_24dp)
                    .into(viewHolder.item_photo_comment_user_img)
        viewHolder.item_photo_comment_text.setText(item.commentText)
        viewHolder.item_photo_comment_userName.setText(item.userName)

        viewHolder.item_photo_comment_userName.setOnClickListener {
            val action = CommentsFragmentDirections.actionCommentsFragmentToOtherProfileFragment(item.userID, null)
            it.findNavController().navigate(action)
        }

    }

    override fun getLayout() = R.layout.item_photo_comment
}
package ivan.ba.myride.view.profile.otherProfile

import android.support.annotation.UiThread
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore

interface OtherProfileMVP {
    interface View {

        @UiThread
        fun showError()

        @UiThread
        fun updatePhotosRecyclerView(items: ArrayList<DMPhotoExplore>)

        @UiThread
        fun updateCarsRecyclerView(items: ArrayList<DMCarProfile>)
        @UiThread
        fun selectCar(position: Int)
    }

    interface Presenter {

        fun setUpView(view: View)
        fun removeView()
        fun showAllPhotos()
        fun showPhotosByCarID(carID: String?)
        fun showSelectedPhotos(carID: String)
        fun getCarsByUserID(userID: String, carID: String?)
        fun getAllPhotos(userID: String, carID: String?)

    }

    interface Model {

        fun getPhotosByUserID(userID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
        fun getPhotosByCarID(carID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
        fun getCarsByUserID(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit)
    }
}
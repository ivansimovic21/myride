package ivan.ba.myride.view.photos.selectCarForPhoto

import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl

class SelectCarForPhotoModel(val repCar: RepCarImpl) : SelectCarForPhotoMVP.Model {
    override fun getCarsOfUser(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit) {
        repCar.getCarsByUserID(userID, onFinish)
    }
}
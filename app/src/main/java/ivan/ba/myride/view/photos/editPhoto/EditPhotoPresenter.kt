package ivan.ba.myride.view.photos.editPhoto

import com.google.firebase.auth.FirebaseAuth
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoInsert
import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto
import ivan.ba.myride.firestoreUtil.StorageUtil
import ivan.ba.myride.util.options.MyOptions

class EditPhotoPresenter(val model: EditPhotoMVP.Model) : EditPhotoMVP.Presenter {
    var view: EditPhotoMVP.View? = null
    override fun setUpView(view: EditPhotoMVP.View) {
        this.view = view
    }

    override fun removeView() {
        this.view = null
    }

    val mPhoto = DMPhotoInsert()
    var mImage: ByteArray? = null

    override fun setData(car: DMCarProfile?, image: ByteArray) {
        if (car != null) {
            mPhoto.carID = car.carID
            mPhoto.carName = car.name
            mPhoto.carImagePath = car.imagePath
        }
        val loggedUser = MyOptions.loggedUser.GET()!!
        mPhoto.userID = FirebaseAuth.getInstance()!!.uid!!
        mPhoto.userName = loggedUser.name
        mPhoto.userImagePath = loggedUser.profilePicturePath
        mImage = image
    }

    override fun uploadPhoto(imageDescription: String, isForProfile: Boolean, isUserProfile: Boolean, refId: String?) {

        StorageUtil.uploadPhoto(mImage!!) {
            if (it != null) {
                if (!isForProfile) {
                    mPhoto.imageDescription = imageDescription
                    mPhoto.imagePath = it
                    model.insertPhoto(mPhoto) {
                        if (it != null) {
                            view?.finish()
                        } else
                            view?.showError()
                    }
                } else {
                    val image = DMProfilePhoto("", refId!!, it)
                    if (isUserProfile)
                        model.insertUserProfilePictures(image) {
                            if (it) {
                                view?.finish()
                            } else
                                view?.showError()
                        }
                    else
                        model.insertCarProfilePictures(image) {
                            if (it) {
                                view?.finish()
                            } else
                                view?.showError()
                        }
                }
            } else
                view?.showError()
        }
    }
}
package ivan.ba.myride.view.photos.photoDetails

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl

class PhotoDetailsModel(val repPhoto: RepPhotoImpl) : PhotoDetailsMVP.Model {


    override fun loadPhoto(photoID: String, onFinish: (DMPhoto?) -> Unit) {
        repPhoto.getPhotoByID(photoID, onFinish)
    }
}
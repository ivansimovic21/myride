package ivan.ba.myride.view.photos.photoDetailsEdit

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl

class PhotoDetailsEditModel(val repPhoto: RepPhotoImpl) : PhotoDetailsEditMVP.Model {


    override fun loadPhoto(photoID: String, onFinish: (DMPhoto?) -> Unit) {
        repPhoto.getPhotoByID(photoID, onFinish)
    }
}
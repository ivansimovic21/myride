package ivan.ba.myride.view.photos.comments

import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl

class CommentsModel(val repPhoto: RepPhotoImpl) : CommentsMVP.Model {
    override fun getCommentsByPhotoID(photoID: String, onFinish: (ArrayList<DMComment>?) -> Unit) {
        repPhoto.getCommentsByPhotoID(photoID, onFinish)
    }

    override fun insertComment(item: DMComment, onFinish: (String?) -> Unit) {
        repPhoto.insertComment(item, onFinish)
    }

    override fun updateComment(item: DMComment, onFinish: (Boolean) -> Unit) {
        repPhoto.updateComment(item, onFinish)
    }

    override fun deleteCommentByID(photoID: String, commentID: String, onFinish: (Boolean) -> Unit) {
        repPhoto.deleteComment(photoID, commentID, onFinish)
    }
}
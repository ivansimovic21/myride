package ivan.ba.myride.view.chat.item

import android.content.Context
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.DMLoggedUser
import ivan.ba.myride.firestoreUtil.StorageUtil
import kotlinx.android.synthetic.main.item_person.*

class PersonItem(val person: DMLoggedUser,
                 val userId: String,
                 private val context: Context)
    : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.textView_name.text = person.name
        if (person.profilePicturePath != null)
            GlideApp.with(context)
                    .load(StorageUtil.pathToReference(person.profilePicturePath))
                    .placeholder(R.drawable.fui_ic_github_white_24dp)
                    .into(viewHolder.imageView_profile_picture)
    }

    override fun getLayout() = R.layout.item_person
}
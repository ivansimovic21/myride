package ivan.ba.myride.view.photos.photoDetailsEdit

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto

interface PhotoDetailsEditMVP{
    interface View {

        fun setUpView(photo: DMPhoto)
        fun loadPhoto(imagePath: String)
        fun showError()
        fun loadCarPhoto(carImagePath: String)
        fun loadUserPhoto(userImagePath: String)
        fun openUserProfile(userID: String, nothing: String?)
    }

    interface Presenter {
        fun setUpView(view: View)
        fun setUpData(photoID: String)
        fun removeView()
        fun loadPhoto()
        fun handleUserClick()
        fun handleCarClick()
    }

    interface Model {
        fun loadPhoto(photoID: String, onFinish: (DMPhoto?) -> Unit)

    }
}
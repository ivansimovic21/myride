package ivan.ba.myride.view.photos.photoDetails

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.data.dataModels.dmPhoto.dmComment.DMComment

interface PhotoDetailsMVP {
    interface View {

        fun setUpView(photo: DMPhoto)
        fun loadUserPhoto(imagePath: String)
        fun loadCarPhoto(imagePath: String)
        fun loadPhoto(imagePath: String)
        fun showError()
        fun openUserProfile(userID: String, carID: String?)
    }

    interface Presenter {
        fun setUpView(view: View)
        fun setUpData(photoID: String)
        fun removeView()
        fun loadPhoto()
        fun handleUserClick()
        fun handleCarClick()
    }

    interface Model {
        fun loadPhoto(photoID: String, onFinish: (DMPhoto?) -> Unit)

    }
}
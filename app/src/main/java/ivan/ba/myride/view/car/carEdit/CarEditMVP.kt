package ivan.ba.myride.view.car.carEdit

import ivan.ba.myride.data.dataModels.dmCar.DMCar

interface CarEditMVP {
    interface View {
        fun success()
        fun showError()
        fun showCar(it: DMCar)

        fun selectImageForCarPhoto(carID: String)
    }

    interface Presenter {

        fun insertCar(carName: String)
        fun setUpView(view: View)
        fun removeView()
        fun loadCarForUpdate(carID: String)
        fun save(carName: String)
        fun updateCar(carName: String)
        fun selectImageForCarPhoto()
    }

    interface Model {
        fun insertCar(carName: String, onFinish: (carID: String?) -> Unit)
        fun updateCar(carID: String, updateCar: String, onFinish: (Boolean) -> Unit)
        fun getCar(carID: String, onFinish: (DMCar?) -> Unit)
    }
}
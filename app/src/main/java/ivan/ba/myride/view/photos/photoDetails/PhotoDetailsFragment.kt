package ivan.ba.myride.view.photos.photoDetails

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhoto
import ivan.ba.myride.firestoreUtil.StorageUtil
import ivan.ba.myride.view.photos.photoExplore.PhotoExploreFragmentDirections
import kotlinx.android.synthetic.main.fragment_photo_details.*
import kotlinx.android.synthetic.main.fragment_photo_details.view.*
import kotlinx.android.synthetic.main.fragment_photo_explore.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject


class PhotoDetailsFragment : Fragment(), PhotoDetailsMVP.View {


    @Inject
    lateinit var presenter: PhotoDetailsMVP.Presenter
    lateinit var mNav : NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_photo_details, container, false)
        MyApp.graph.inject(this)
        mNav = findNavController()
        presenter.setUpView(this)
        val photoID = PhotoDetailsFragmentArgs.fromBundle(arguments).photoID
        presenter.setUpData(photoID)
        presenter.loadPhoto()

        view.photo_details_txt_comments.setOnClickListener {
            val action = PhotoDetailsFragmentDirections.actionPhotoDetailsFragmentToCommentsFragment(photoID)
            mNav.navigate(action)
        }
        view.photo_details_btn_back.setOnClickListener {
            mNav.popBackStack()
        }
        view.photo_details_rl_header_user.setOnClickListener {
            presenter.handleUserClick()
        }
        view.photo_details_rl_header_car.setOnClickListener {
            presenter.handleCarClick()
        }

        return view
    }

    override fun openUserProfile(userID: String, carID: String?) {
        val action = PhotoDetailsFragmentDirections.actionPhotoDetailsFragmentToOtherProfileFragment(userID, carID)
        mNav.navigate(action)
    }

    override fun setUpView(photo: DMPhoto) {
        photo_details_txt_car.text = photo.carName
        photo_details_txt_user.text = photo.userName
        photo_details_txt_likes.text = photo.likes.toString()
        photo_details_txt_comments.text = photo.comments.toString()
        photo_details_txt_description.text = photo.imageDescription
    }

    override fun loadUserPhoto(imagePath: String) {
        GlideApp.with(this)
                .load(StorageUtil.pathToReference(imagePath))
                .placeholder(R.drawable.ic_directions_car_black_24dp)
                .into(photo_details_img_user)
    }

    override fun loadCarPhoto(imagePath: String) {
        GlideApp.with(this)
                .load(StorageUtil.pathToReference(imagePath))
                .placeholder(R.drawable.ic_directions_car_black_24dp)
                .into(photo_details_img_car)
    }

    override fun loadPhoto(imagePath: String) {
        GlideApp.with(this)
                .load(StorageUtil.pathToReference(imagePath))
                .placeholder(R.drawable.ic_directions_car_black_24dp)
                .into(photo_details_img_photo)
    }


    @UiThread
    override fun showError() {
        longSnackbar(photo_explore_rl, "Network error")
    }


    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }

}

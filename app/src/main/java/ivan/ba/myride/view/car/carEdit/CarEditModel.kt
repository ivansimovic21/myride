package ivan.ba.myride.view.car.carEdit

import com.google.firebase.auth.FirebaseAuth
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.data.dataModels.dmCar.DMCarInsert
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl

class CarEditModel(val repCar: RepCarImpl) : CarEditMVP.Model {


    override fun getCar(carID: String, onFinish: (DMCar?) -> Unit) {
        repCar.getCarByID(carID, onFinish)
    }

    override fun insertCar(carName: String, onFinish: (carID: String?) -> Unit) {
        repCar.insertCar(DMCarInsert(carName, FirebaseAuth.getInstance()!!.uid!!), onFinish)
    }

    override fun updateCar(carID: String, carName: String, onFinish: (Boolean) -> Unit) {
        repCar.updateCar(DMCar(carID,"",carName,""),onFinish)
    }
}
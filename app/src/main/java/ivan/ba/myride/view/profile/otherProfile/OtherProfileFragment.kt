package ivan.ba.myride.view.profile.otherProfile

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.OnItemClickListener
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.view.profile.adapters.CarsProfileAdapterItem
import ivan.ba.myride.view.profile.adapters.PhotosProfileAdapterItem
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.fragment_other_profile.*
import kotlinx.android.synthetic.main.fragment_other_profile.view.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class OtherProfileFragment : Fragment(), OtherProfileMVP.View {

    @Inject
    lateinit var presenter: OtherProfileMVP.Presenter

    private var shouldInitRecyclerViewCars = true
    private lateinit var sectionCars: Section

    private var shouldInitRecyclerViewPhoto = true
    private lateinit var sectionPhoto: Section

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_other_profile, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        presenter.getAllPhotos(OtherProfileFragmentArgs.fromBundle(arguments).userId, OtherProfileFragmentArgs.fromBundle(arguments).carId)

        view.other_profile_btn_car.setOnClickListener {

            shouldInitRecyclerViewCars = true
            val action = OtherProfileFragmentDirections.actionOtherProfileFragmentToCarsOfUserFragment(OtherProfileFragmentArgs.fromBundle(arguments).userId,false)
            it.findNavController().navigate(action)
        }

        view.other_profile_img_profilePic.setOnClickListener {

            shouldInitRecyclerViewCars = true
            val action = OtherProfileFragmentDirections.actionOtherProfileFragmentToProfilePhotoFragment(false,true,OtherProfileFragmentArgs.fromBundle(arguments).userId)
            it.findNavController().navigate(action)
        }

        return view
    }

    @UiThread
    override fun updateCarsRecyclerView(items: ArrayList<DMCarProfile>) {

        val listOfCarsAdapter = ArrayList<CarsProfileAdapterItem>()
        for (i in items) {
            listOfCarsAdapter.add(CarsProfileAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerViewCars) {
            other_profile_rw_cars.apply {
                layoutManager = LinearLayoutManager(this@OtherProfileFragment.context, LinearLayoutManager.HORIZONTAL, false)
                adapter = GroupAdapter<ViewHolder>().apply {
                    sectionCars = Section(listOfCarsAdapter)
                    add(sectionCars)
                    setOnItemClickListener(onCarItemClick)
                }
            }
            shouldInitRecyclerViewCars = false
        } else
            sectionCars.update(listOfCarsAdapter)

    }

    private val onCarItemClick = OnItemClickListener { item, view ->
        if (item is CarsProfileAdapterItem) {
            presenter.showSelectedPhotos(item.car.carID)
        }
    }

    override fun selectCar(position: Int) {
        other_profile_rw_photos.findViewHolderForAdapterPosition(position).itemView.performClick()
    }

    @UiThread
    override fun updatePhotosRecyclerView(items: ArrayList<DMPhotoExplore>) {

        val listOfCarsAdapter = ArrayList<PhotosProfileAdapterItem>()
        for (i in items) {
            listOfCarsAdapter.add(PhotosProfileAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerViewPhoto) {
            other_profile_rw_photos.apply {
                layoutManager = GridLayoutManager(this@OtherProfileFragment.context, 2)
                adapter = GroupAdapter<ViewHolder>().apply {
                    sectionPhoto = Section(listOfCarsAdapter)
                    add(sectionPhoto)
                    setOnItemClickListener(onPhotoItemClick)
                }
            }
            shouldInitRecyclerViewPhoto = false
        } else
            sectionPhoto.update(listOfCarsAdapter)

    }

    @UiThread
    override fun showError() {
        longSnackbar(myProfile_cl, "NETWORK ERROR")
    }

    private val onPhotoItemClick = OnItemClickListener { item, view ->
        if (item is PhotosProfileAdapterItem) {

        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }

}

package ivan.ba.myride.view.profile.profilePictures

class ProfilePhotoPresenter(val model: ProfilePhotoMVP.Model) : ProfilePhotoMVP.Presenter {

    var view: ProfilePhotoMVP.View? = null
    override fun setUpView(view: ProfilePhotoMVP.View) {
        this.view = view
    }

    override fun removeView() {
        view = null
    }

    override fun loadData(refId: String, isUser: Boolean) {
        if(isUser)
            model.getUserProfilePhotos(refId){
                if(it != null)
                    view?.updateRecyclerView(it)
                else
                    view?.showError()
            }
        else
            model.getCarProfilePhotos(refId){
                if(it != null)
                    view?.updateRecyclerView(it)
                else
                    view?.showError()
            }
    }


}
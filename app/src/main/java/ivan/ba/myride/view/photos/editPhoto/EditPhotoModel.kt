package ivan.ba.myride.view.photos.editPhoto

import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoInsert
import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl
import ivan.ba.myride.data.reposetoryImpl.RepProfilePhotoImpl

class EditPhotoModel(val repUser: RepPhotoImpl, val repProfilePhoto: RepProfilePhotoImpl) : EditPhotoMVP.Model {
    override fun insertPhoto(mPhoto: DMPhotoInsert, onFinish: (String?) -> Unit) {
        repUser.insertPhoto(mPhoto, onFinish)
    }

    override fun insertUserProfilePictures(item: DMProfilePhoto, onFinish: (Boolean) -> Unit){
        repProfilePhoto.insertUserProfilePictures(item,onFinish)
    }

    override fun insertCarProfilePictures(item: DMProfilePhoto, onFinish: (Boolean) -> Unit){
        repProfilePhoto.insertCarProfilePictures(item,onFinish)
    }
}
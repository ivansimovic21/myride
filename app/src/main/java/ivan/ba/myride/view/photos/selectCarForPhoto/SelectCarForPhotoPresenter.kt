package ivan.ba.myride.view.photos.selectCarForPhoto

import com.google.firebase.auth.FirebaseAuth
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.view.car.carsOfUser.CarsOfUserMVP

class SelectCarForPhotoPresenter(val model: SelectCarForPhotoMVP.Model) : SelectCarForPhotoMVP.Presenter {
    var view: SelectCarForPhotoMVP.View? = null
    override fun setUpView(view: SelectCarForPhotoMVP.View) {
        this.view = view
    }

    override fun removeView() {
        this.view = null
    }


    override fun getCarsOfUser() {
        model.getCarsOfUser(FirebaseAuth.getInstance()!!.uid!!) {
            if (it != null) {
                view?.updateRecyclerView(it)
            } else
                view?.showError()
        }
    }
}
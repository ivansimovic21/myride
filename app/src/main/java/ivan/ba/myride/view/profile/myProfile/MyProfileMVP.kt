package ivan.ba.myride.view.profile.myProfile

import android.support.annotation.UiThread
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.data.dataModels.dmProfile.DMProfile

interface MyProfileMVP {
    interface View {

        @UiThread
        fun updateCarsRecyclerView(items: ArrayList<DMCarProfile>)

        @UiThread
        fun updatePhotosRecyclerView(items: ArrayList<DMPhotoExplore>)

        fun showError()
        fun setUpData(imagePath: String)
    }

    interface Presenter {

        fun showAllPhotos()
        fun getCarsByUserID()
        fun showPhotosByCarID(carID: String?)
        fun setUpView(view: View)
        fun removeView()
        fun showSelectedPhotos(carID: String)
        fun getAllPhotos()
        fun loadData()
    }

    interface Model {

        fun getPhotosByUserID(userID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
        fun getPhotosByCarID(carID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit)
        fun getCarsByUserID(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit)
        fun getProfileByID(userID: String, onFinish: (DMProfile?) -> Unit)
    }
}
package ivan.ba.myride.view.chat.chat

interface ChatMVP {
    interface View {

    }

    interface Presenter {

    }

    interface Model {

    }
}
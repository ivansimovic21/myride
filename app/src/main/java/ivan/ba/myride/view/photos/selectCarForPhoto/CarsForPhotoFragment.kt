package ivan.ba.myride.view.photos.selectCarForPhoto

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.OnItemClickListener
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.view.car.adapters.CarsOfUserAdapterItem
import kotlinx.android.synthetic.main.fragment_cars_for_photo.*
import kotlinx.android.synthetic.main.fragment_cars_for_photo.view.*
import kotlinx.android.synthetic.main.fragment_cars_of_user.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class CarsForPhotoFragment : Fragment(), SelectCarForPhotoMVP.View {

    private var shouldInitRecyclerView = true
    private lateinit var section: Section

    @Inject
    lateinit var presenter: SelectCarForPhotoMVP.Presenter
    lateinit var mNav: NavController
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_cars_for_photo, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        mNav = findNavController()
        view.select_car_for_photo_btn_add.setOnClickListener {
            shouldInitRecyclerView = true
            val action = CarsForPhotoFragmentDirections.actionCarsForPhotoFragmentToCarEditFragment("")
            mNav.navigate(action)
        }
        view.select_car_for_photo_btn_back.setOnClickListener {
            mNav.popBackStack()
        }
        view.select_car_for_photo_btn_no_car.setOnClickListener {
            onNoCarClick()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.getCarsOfUser()
    }

    @UiThread
    override fun showError() {
        longSnackbar(cars_of_users_rl, "No network")
    }

    @UiThread
    override fun updateRecyclerView(items: ArrayList<DMCarProfile>) {

        val listOfCarsAdapter = ArrayList<CarsOfUserAdapterItem>()
        for (i in items) {
            listOfCarsAdapter.add(CarsOfUserAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerView) {
            select_car_for_photo_rv.apply {
                layoutManager = LinearLayoutManager(this@CarsForPhotoFragment.context)
                adapter = GroupAdapter<ViewHolder>().apply {
                    section = Section(listOfCarsAdapter)
                    add(section)
                    setOnItemClickListener(onItemClick)
                }
            }
            shouldInitRecyclerView = false
        } else
            section.update(listOfCarsAdapter)

    }

    fun onNoCarClick(){
        shouldInitRecyclerView = true
        val action = CarsForPhotoFragmentDirections.actionCarsForPhotoFragmentToTakePhotoFragment(null,false,false,null)
        mNav.navigate(action)
    }

    private val onItemClick = OnItemClickListener { item, view ->
        if (item is CarsOfUserAdapterItem) {
            shouldInitRecyclerView = true
            val action = CarsForPhotoFragmentDirections.actionCarsForPhotoFragmentToTakePhotoFragment(item.car,false,false,null)
            mNav.navigate(action)
        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }
}

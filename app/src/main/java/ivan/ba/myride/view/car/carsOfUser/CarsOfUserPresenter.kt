package ivan.ba.myride.view.car.carsOfUser

import ivan.ba.myride.data.dataModels.dmCar.DMCar

class CarsOfUserPresenter(val model: CarsOfUserMVP.Model) : CarsOfUserMVP.Presenter {

    var view: CarsOfUserMVP.View? = null
    override fun setUpView(view: CarsOfUserMVP.View) {
        this.view = view
    }

    override fun removeView() {
        this.view = null
    }

    lateinit var mUserID: String

    override fun setUserID(userID: String) {
        this.mUserID = userID
    }

    override fun getCarsOfUser() {
        model.getCarsOfUser(mUserID) {
            if (it != null) {
                view?.updateRecyclerView(it)
            } else
                view?.showError()
        }
    }
}
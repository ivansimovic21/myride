package ivan.ba.myride.view.profile.myProfile

import com.google.firebase.auth.FirebaseAuth
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import java.util.*
import kotlin.collections.ArrayList

class MyProfilePresenter(val model: MyProfileMVP.Model) : MyProfileMVP.Presenter {

    lateinit var mPhotos: ArrayList<DMPhotoExplore>

    var view: MyProfileMVP.View? = null
    override fun setUpView(view: MyProfileMVP.View) {
        this.view = view
    }

    override fun removeView() {
        this.view = null
    }

    override fun loadData() {
        model.getProfileByID(FirebaseAuth.getInstance()!!.uid!!){
            if(it != null)
                view?.setUpData(it.imagePath)
            else
                view?.showError()
        }
    }

    override fun getAllPhotos(){
        model.getPhotosByUserID(FirebaseAuth.getInstance().uid!!) {
            if (it != null) {
                mPhotos = it
                getCarsByUserID()
                view?.updatePhotosRecyclerView(it)
            } else
                view?.showError()
        }
    }

    override fun showAllPhotos() {
        view?.updatePhotosRecyclerView(mPhotos)
    }

    override fun showPhotosByCarID(carID: String?) {
        view?.updatePhotosRecyclerView(mPhotos.filter { x -> x.carID == carID } as ArrayList<DMPhotoExplore>)
    }

    override fun showSelectedPhotos(carID: String){
        if(carID.equals("all"))
            showAllPhotos()
        else{
            val mCarID :String?
            if(carID.equals(""))
                mCarID = null
            else
                mCarID = carID
            showPhotosByCarID(mCarID)
        }
    }

    override fun getCarsByUserID() {
        model.getCarsByUserID(FirebaseAuth.getInstance().uid!!) {
            if (it != null) {
                val countPhotosWithoutCar = mPhotos.filter { x -> x.carID == null }.size
                if(countPhotosWithoutCar != 0 && countPhotosWithoutCar != mPhotos.size){
                    it.add(0, DMCarProfile("","","No car",""))
                }
                it.add(0, DMCarProfile("all","","All",""))

                view?.updateCarsRecyclerView(it)
            } else
                view?.showError()
        }
    }
}
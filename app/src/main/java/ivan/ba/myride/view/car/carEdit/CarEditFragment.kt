package ivan.ba.myride.view.car.carEdit

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.firestoreUtil.StorageUtil
import kotlinx.android.synthetic.main.fragment_car_edit.*
import kotlinx.android.synthetic.main.fragment_car_edit.view.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class CarEditFragment : Fragment(), CarEditMVP.View {


    var isForPhoto = false
    var isUpdate = false

    @Inject
    lateinit var presenter: CarEditMVP.Presenter
    lateinit var mNav: NavController

    lateinit var mView: View


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_car_edit, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        val carID = CarEditFragmentArgs.fromBundle(arguments).carID
        if (carID != "") {
            presenter.loadCarForUpdate(carID)
            isUpdate = true
        }

        isForPhoto = CarEditFragmentArgs.fromBundle(arguments).isForNewPhoto.toBoolean()

        mNav = findNavController()
        mView.carEdit_btn_back.setOnClickListener {
            mNav.popBackStack()
        }
        mView.carEdit_btn_save.setOnClickListener {
            if (!mView.carEdit_text_name.text.toString().isEmpty())
                presenter.save(mView.carEdit_text_name.text.toString())
        }
        mView.carEdit_img_carPic.setOnClickListener {
            presenter.selectImageForCarPhoto()
        }
        return mView
    }

    override fun selectImageForCarPhoto(carID: String) {
        val action = CarEditFragmentDirections.actionCarEditFragmentToTakePhotoFragment(null, true, false, carID)
        mNav.navigate(action)
    }

    override fun showCar(it: DMCar) {
        mView.carEdit_img_carPic.visibility = VISIBLE
        mView.carEdit_text_name.setText(it.name)
        if (it.imagePath != "")
            GlideApp.with(context!!)
                    .load(StorageUtil.pathToReference(it.imagePath))
                    .placeholder(R.drawable.ic_directions_car_black_24dp)
                    .into(mView.carEdit_img_carPic)
    }

    override fun success() {
        if (!isUpdate) {
            isUpdate = true
            mView.carEdit_img_carPic.visibility = VISIBLE
        }
    }

    override fun showError() {
        longSnackbar(carEdit_cl, "Network error")
    }

    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }

}

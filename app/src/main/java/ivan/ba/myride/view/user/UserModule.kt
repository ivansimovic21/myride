
package ivan.ba.myride.view.user

import dagger.Module
import dagger.Provides
import ivan.ba.myride.data.reposetoryImpl.RepUserImpl
import ivan.ba.myride.view.user.report.ReportMVP
import ivan.ba.myride.view.user.report.ReportModel
import ivan.ba.myride.view.user.report.ReportPresenter
import ivan.ba.myride.view.user.settings.SettingsMVP
import ivan.ba.myride.view.user.settings.SettingsModel
import ivan.ba.myride.view.user.settings.SettingsPresenter
import javax.inject.Singleton

@Module
class UserModule {

    @Provides
    @Singleton
    fun provideSettingsModel(rep: RepUserImpl): SettingsMVP.Model {
        return SettingsModel(rep)
    }

    @Provides
    fun provideSettingsPresenter(model: SettingsMVP.Model): SettingsMVP.Presenter {
        return SettingsPresenter(model)
    }

    @Provides
    @Singleton
    fun provideReportModel(rep: RepUserImpl): ReportMVP.Model {
        return ReportModel(rep)
    }

    @Provides
    fun provideReportPresenter(model: ReportMVP.Model): ReportMVP.Presenter {
        return ReportPresenter(model)
    }
}


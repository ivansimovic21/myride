package ivan.ba.myride.view.photos.photoExplore

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.OnItemClickListener
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.view.photos.adapters.PhotosExploreAdapterItem
import kotlinx.android.synthetic.main.fragment_photo_explore.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class PhotoExploreFragment : Fragment(), PhotoExploreMVP.View {

    @Inject
    lateinit var presenter: PhotoExploreMVP.Presenter
    private var shouldInitRecyclerViewCars = true
    private lateinit var sectionCars: Section
    lateinit var mNav: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_photo_explore, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        mNav = findNavController()
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.getPhotos()
    }


    @UiThread
    override fun updateRecyclerView(items: ArrayList<DMPhotoExplore>) {

        val listOfAdapter = ArrayList<PhotosExploreAdapterItem>()
        for (i in items) {
            listOfAdapter.add(PhotosExploreAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerViewCars) {
            photo_explore_rw.apply {
                layoutManager = GridLayoutManager(this@PhotoExploreFragment.context, 2)
                adapter = GroupAdapter<ViewHolder>().apply {
                    sectionCars = Section(listOfAdapter)
                    add(sectionCars)
                    setOnItemClickListener(onCarItemClick)
                }
            }
            shouldInitRecyclerViewCars = false
        } else
            sectionCars.update(listOfAdapter)

    }

    @UiThread
    override fun showError() {
        longSnackbar(photo_explore_rl, "Network error")
    }

    private val onCarItemClick = OnItemClickListener { item, view ->
        if (item is PhotosExploreAdapterItem) {
            shouldInitRecyclerViewCars = true
            val action = PhotoExploreFragmentDirections.actionPhotoExploreFragmentToPhotoDetailsFragment(item.item.photoID)
            mNav.navigate(action)
        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }

}

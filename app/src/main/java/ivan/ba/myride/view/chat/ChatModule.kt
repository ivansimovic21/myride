package ivan.ba.myride.view.chat

import dagger.Module
import dagger.Provides
import ivan.ba.myride.data.reposetory.RepChat
import ivan.ba.myride.data.reposetoryImpl.RepChatImpl
import ivan.ba.myride.view.chat.chat.ChatMVP
import ivan.ba.myride.view.chat.chat.ChatModel
import ivan.ba.myride.view.chat.chat.ChatPresenter
import ivan.ba.myride.view.chat.contacts.ContactsMVP
import ivan.ba.myride.view.chat.contacts.ContactsModel
import ivan.ba.myride.view.chat.contacts.ContactsPresenter
import javax.inject.Singleton

@Module
class ChatModule {
    @Provides
    @Singleton
    fun provideChatModel(rep: RepChatImpl): ChatMVP.Model {
        return ChatModel(rep)
    }

    @Provides
    fun provideChatPresenter(model: ChatMVP.Model): ChatMVP.Presenter {
        return ChatPresenter(model)
    }

    @Provides
    @Singleton
    fun provideContactsModel(rep: RepChatImpl): ContactsMVP.Model {
        return ContactsModel(rep)
    }

    @Provides
    fun provideContactsPresenter(model: ContactsMVP.Model): ContactsMVP.Presenter {
        return ContactsPresenter(model)
    }

    @Provides
    @Singleton
    fun provideChatRepository(): RepChatImpl {
        return RepChat()
    }
}
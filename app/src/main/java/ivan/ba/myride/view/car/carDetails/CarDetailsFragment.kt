package ivan.ba.myride.view.car.carDetails

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmCar.DMCar
import ivan.ba.myride.firestoreUtil.StorageUtil
import kotlinx.android.synthetic.main.fragment_car_details.view.*
import kotlinx.android.synthetic.main.fragment_car_edit.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class CarDetailsFragment : Fragment(), CarDetailsMVP.View {
    @Inject
    lateinit var presenter: CarDetailsMVP.Presenter
    lateinit var mNav: NavController

    lateinit var mView: View


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_car_details, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        val carID = CarDetailsFragmentArgs.fromBundle(arguments).carId
        if (carID != "")
            presenter.loadCar(carID)
        mNav = findNavController()
        mView.carDetails_btn_back.setOnClickListener {
            mNav.popBackStack()
        }
        mView.carDetails_img_carPic.setOnClickListener {
            val action = CarDetailsFragmentDirections.actionCarDetailsFragmentToProfilePhotoFragment(false,false,carID)
            mNav.navigate(action)
        }
        return mView
    }

    override fun showCar(it: DMCar) {
        mView.carDetails_text_name.setText(it.name)
        if (it.imagePath != "")
            GlideApp.with(context!!)
                    .load(StorageUtil.pathToReference(it.imagePath))
                    .placeholder(R.drawable.ic_directions_car_black_24dp)
                    .into(mView.carDetails_img_carPic)
    }

    override fun showError() {
        longSnackbar(carEdit_cl, "Network error")
    }

    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }
}

package ivan.ba.myride.view.car

import dagger.Module
import dagger.Provides
import ivan.ba.myride.data.reposetory.RepCar
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl
import ivan.ba.myride.view.car.carDetails.CarDetailsMVP
import ivan.ba.myride.view.car.carDetails.CarDetailsModel
import ivan.ba.myride.view.car.carDetails.CarDetailsPresenter
import ivan.ba.myride.view.car.carEdit.CarEditMVP
import ivan.ba.myride.view.car.carEdit.CarEditModel
import ivan.ba.myride.view.car.carEdit.CarEditPresenter
import ivan.ba.myride.view.car.carFilter.CarFilterMVP
import ivan.ba.myride.view.car.carFilter.CarFilterModel
import ivan.ba.myride.view.car.carFilter.CarFilterPresenter
import ivan.ba.myride.view.car.carPhotosForExplore.CarPhotosForExploreMVP
import ivan.ba.myride.view.car.carPhotosForExplore.CarPhotosForExploreModel
import ivan.ba.myride.view.car.carPhotosForExplore.CarPhotosForExplorePresenter
import ivan.ba.myride.view.car.carPhotosForSale.CarPhotosForSaleMVP
import ivan.ba.myride.view.car.carPhotosForSale.CarPhotosForSaleModel
import ivan.ba.myride.view.car.carPhotosForSale.CarPhotosForSalePresenter
import ivan.ba.myride.view.car.carsOfUser.CarsOfUserMVP
import ivan.ba.myride.view.car.carsOfUser.CarsOfUserModel
import ivan.ba.myride.view.car.carsOfUser.CarsOfUserPresenter
import ivan.ba.myride.view.car.carsOnExplore.CarsOnExploreMVP
import ivan.ba.myride.view.car.carsOnExplore.CarsOnExploreModel
import ivan.ba.myride.view.car.carsOnExplore.CarsOnExplorePresenter
import ivan.ba.myride.view.car.carsOnSale.CarsOnSaleMVP
import ivan.ba.myride.view.car.carsOnSale.CarsOnSaleModel
import ivan.ba.myride.view.car.carsOnSale.CarsOnSalePresenter
import javax.inject.Singleton

@Module
class CarModule {

    @Provides
    @Singleton
    fun provideCarDetailsModel(rep: RepCarImpl): CarDetailsMVP.Model {
        return CarDetailsModel(rep)
    }

    @Provides
    fun provideCarDetailsPresenter(model: CarDetailsMVP.Model): CarDetailsMVP.Presenter {
        return CarDetailsPresenter(model)
    }

    @Provides
    @Singleton
    fun provideCarEditModel(rep: RepCarImpl): CarEditMVP.Model {
        return CarEditModel(rep)
    }

    @Provides
    fun provideCarEditPresenter(model: CarEditMVP.Model): CarEditMVP.Presenter {
        return CarEditPresenter(model)
    }

    @Provides
    @Singleton
    fun provideCarPhotosForExploreModel(rep: RepCarImpl): CarPhotosForExploreMVP.Model {
        return CarPhotosForExploreModel(rep)
    }

    @Provides
    fun provideCarPhotosForExplorePresenter(model: CarPhotosForExploreMVP.Model): CarPhotosForExploreMVP.Presenter {
        return CarPhotosForExplorePresenter(model)
    }

    @Provides
    @Singleton
    fun provideCarPhotosForSaleModel(rep: RepCarImpl): CarPhotosForSaleMVP.Model {
        return CarPhotosForSaleModel(rep)
    }

    @Provides
    fun provideCarPhotosForSalePresenter(model: CarPhotosForSaleMVP.Model): CarPhotosForSaleMVP.Presenter {
        return CarPhotosForSalePresenter(model)
    }

    @Provides
    @Singleton
    fun provideCarsOfUserModel(rep: RepCarImpl): CarsOfUserMVP.Model {
        return CarsOfUserModel(rep)
    }

    @Provides
    fun provideCarsOfUserPresenter(model: CarsOfUserMVP.Model): CarsOfUserMVP.Presenter {
        return CarsOfUserPresenter(model)
    }

    @Provides
    @Singleton
    fun provideCarsOnExploreModel(rep: RepCarImpl): CarsOnExploreMVP.Model {
        return CarsOnExploreModel(rep)
    }

    @Provides
    fun provideCarsOnExplorePresenter(model: CarsOnExploreMVP.Model): CarsOnExploreMVP.Presenter {
        return CarsOnExplorePresenter(model)
    }

    @Provides
    @Singleton
    fun provideCarsOnSaleModel(rep: RepCarImpl): CarsOnSaleMVP.Model {
        return CarsOnSaleModel(rep)
    }

    @Provides
    fun provideCarsOnSalePresenter(model: CarsOnSaleMVP.Model): CarsOnSaleMVP.Presenter {
        return CarsOnSalePresenter(model)
    }

    @Provides
    @Singleton
    fun provideCarsFilterModel(rep: RepCarImpl): CarFilterMVP.Model {
        return CarFilterModel(rep)
    }

    @Provides
    fun provideCarsFilterPresenter(model: CarFilterMVP.Model): CarFilterMVP.Presenter {
        return CarFilterPresenter(model)
    }

    @Provides
    @Singleton
    fun provideCarRepository(): RepCarImpl {
        return RepCar()
    }
}

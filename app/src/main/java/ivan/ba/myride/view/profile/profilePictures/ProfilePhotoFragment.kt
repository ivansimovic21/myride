package ivan.ba.myride.view.profile.profilePictures

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.OnItemClickListener
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto
import ivan.ba.myride.view.profile.adapters.ProfilePhotosAdapterItem
import kotlinx.android.synthetic.main.fragment_profile_photos.*
import kotlinx.android.synthetic.main.fragment_profile_photos.view.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class ProfilePhotoFragment : Fragment(), ProfilePhotoMVP.View {

    @Inject
    lateinit var presenter: ProfilePhotoMVP.Presenter
    private var shouldInitRecyclerViewCars = true
    private lateinit var sectionPhotos: Section
    lateinit var mNav: NavController

    var isUpdatable = false
    var isUser = false
    var refId = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profile_photos, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        mNav = findNavController()
        isUpdatable = ProfilePhotoFragmentArgs.fromBundle(arguments).isUpdatable
        isUser = ProfilePhotoFragmentArgs.fromBundle(arguments).isUser
        refId = ProfilePhotoFragmentArgs.fromBundle(arguments).refId
        presenter.loadData(refId, isUser)

        view.profile_picture_btn_new.setOnClickListener {
            val action = ProfilePhotoFragmentDirections.actionProfilePhotoFragmentToTakePhotoFragment(null,true,isUser,refId)
            mNav.navigate(action)
        }
        view.profile_picture_btn_back.setOnClickListener {
            mNav.popBackStack()
        }

        return view
    }

    @UiThread
    override fun updateRecyclerView(items: ArrayList<DMProfilePhoto>) {

        val listOfAdapter = ArrayList<ProfilePhotosAdapterItem>()
        for (i in items) {
            listOfAdapter.add(ProfilePhotosAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerViewCars) {
            profile_picture_rw.apply {
                layoutManager = GridLayoutManager(this.context, 2)
                adapter = GroupAdapter<ViewHolder>().apply {
                    sectionPhotos = Section(listOfAdapter)
                    add(sectionPhotos)
                    setOnItemClickListener(onPhotoClick)
                }
            }
            shouldInitRecyclerViewCars = false
        } else
            sectionPhotos.update(listOfAdapter)

    }

    @UiThread
    override fun showError() {
        longSnackbar(photo_explore_rl, "Network error")
    }

    private val onPhotoClick = OnItemClickListener { item, view ->
        if (item is ProfilePhotosAdapterItem) {
        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }
}

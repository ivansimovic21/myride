package ivan.ba.myride.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.Navigation
import com.google.firebase.auth.FirebaseAuth
import ivan.ba.myride.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController = Navigation.findNavController(this, R.id.main_container)
        main_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_main -> {
                    navController.navigate(R.id.photoExploreFragment)
                    true
                }
                R.id.navigation_cars -> {
                    navController.navigate(R.id.carsOnExploreFragment)
                    true
                }
                R.id.navigation_chat -> {
                    navController.navigate(R.id.chatFragment)
                    true
                }
                R.id.navigation_profile -> {
                    if (FirebaseAuth.getInstance().currentUser == null)
                        navController.navigate(R.id.loginFragment)
                    else
                        navController.navigate(R.id.myProfileFragment)
                    true
                }
                else -> false
            }
        }
    }
}

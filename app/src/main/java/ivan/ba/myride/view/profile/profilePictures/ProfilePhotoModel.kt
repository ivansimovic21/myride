package ivan.ba.myride.view.profile.profilePictures

import ivan.ba.myride.data.dataModels.dmProfilePicture.DMProfilePhoto
import ivan.ba.myride.data.reposetoryImpl.RepProfilePhotoImpl

class ProfilePhotoModel(val repProfilePhoto: RepProfilePhotoImpl) : ProfilePhotoMVP.Model {
    override fun getUserProfilePhotos(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit) {
        repProfilePhoto.getUserProfilePictures(userID, onFinish)
    }

    override fun insertUserProfilePhotos(item: DMProfilePhoto, onFinish: (Boolean) -> Unit) {
        repProfilePhoto.insertUserProfilePictures(item, onFinish)
    }

    override fun getCarProfilePhotos(userID: String, onFinish: (ArrayList<DMProfilePhoto>?) -> Unit) {
        repProfilePhoto.getCarProfilePictures(userID, onFinish)
    }

    override fun insertCarProfilePhotos(item: DMProfilePhoto, onFinish: (Boolean) -> Unit) {
        repProfilePhoto.insertCarProfilePictures(item, onFinish)
    }


}
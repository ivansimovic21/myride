package ivan.ba.myride.view.profile.myProfile

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.OnItemClickListener
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import ivan.ba.firebasetemplate.glide.GlideApp
import ivan.ba.myride.MyApp
import ivan.ba.myride.R
import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.firestoreUtil.StorageUtil
import ivan.ba.myride.view.profile.adapters.CarsProfileAdapterItem
import ivan.ba.myride.view.profile.adapters.PhotosProfileAdapterItem
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.fragment_my_profile.view.*
import kotlinx.android.synthetic.main.item_photo_explore.*
import org.jetbrains.anko.design.longSnackbar
import javax.inject.Inject

class MyProfileFragment : Fragment(), MyProfileMVP.View {
    @Inject
    lateinit var presenter: MyProfileMVP.Presenter

    private var shouldInitRecyclerViewCars = true
    private lateinit var sectionCars: Section

    private var shouldInitRecyclerViewPhoto = true
    private lateinit var sectionPhoto: Section

    lateinit var mNav: NavController
    lateinit var mView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_my_profile, container, false)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        mNav = findNavController()
        mView.myProfile_btn_addImage.setOnClickListener {
            shouldInitRecyclerViewCars = true
            mNav.navigate(R.id.carsForPhotoFragment)
        }
        mView.myProfile_btn_car.setOnClickListener {
            shouldInitRecyclerViewCars = true
            val action = MyProfileFragmentDirections.actionMyProfileFragmentToCarsOfUserFragment(FirebaseAuth.getInstance()!!.uid!!, true)
            mNav.navigate(action)
        }
        mView.myProfile_img_profilePic.setOnClickListener {
            shouldInitRecyclerViewCars = true
            val action = MyProfileFragmentDirections.actionMyProfileFragmentToProfilePhotoFragment(true, true, FirebaseAuth.getInstance()!!.uid!!)
            it.findNavController().navigate(action)
        }
        mView.myProfile_btn_menu.setOnClickListener {
            shouldInitRecyclerViewCars = true
            AuthUI.getInstance().signOut(activity!!.applicationContext);
            mNav.navigate(R.id.loginFragment)
        }
        return mView
    }

    override fun setUpData(imagePath: String) {
        GlideApp.with(context!!)
                .load(StorageUtil.pathToReference(imagePath))
                .placeholder(R.drawable.ic_directions_car_black_24dp)
                .into(mView.myProfile_img_profilePic)
    }

    override fun onResume() {
        super.onResume()
        presenter.loadData()
        presenter.getAllPhotos()
    }


    @UiThread
    override fun updateCarsRecyclerView(items: ArrayList<DMCarProfile>) {

        val listOfCarsAdapter = ArrayList<CarsProfileAdapterItem>()
        for (i in items) {
            listOfCarsAdapter.add(CarsProfileAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerViewCars) {
            my_profile_rw_cars.apply {
                layoutManager = LinearLayoutManager(this@MyProfileFragment.context, LinearLayoutManager.HORIZONTAL, false)
                adapter = GroupAdapter<ViewHolder>().apply {
                    sectionCars = Section(listOfCarsAdapter)
                    add(sectionCars)
                    setOnItemClickListener(onCarItemClick)
                }
            }
            shouldInitRecyclerViewCars = false
        } else
            sectionCars.update(listOfCarsAdapter)

    }

    private val onCarItemClick = OnItemClickListener { item, view ->
        if (item is CarsProfileAdapterItem) {
            presenter.showSelectedPhotos(item.car.carID)
        }
    }

    @UiThread
    override fun updatePhotosRecyclerView(items: ArrayList<DMPhotoExplore>) {

        val listOfCarsAdapter = ArrayList<PhotosProfileAdapterItem>()
        for (i in items) {
            listOfCarsAdapter.add(PhotosProfileAdapterItem(i, context!!))
        }

        if (shouldInitRecyclerViewPhoto) {
            my_profile_rw_photos.apply {
                layoutManager = GridLayoutManager(this@MyProfileFragment.context, 2)
                adapter = GroupAdapter<ViewHolder>().apply {
                    sectionPhoto = Section(listOfCarsAdapter)
                    add(sectionPhoto)
                    setOnItemClickListener(onPhotoItemClick)
                }
            }
            shouldInitRecyclerViewPhoto = false
        } else
            sectionPhoto.update(listOfCarsAdapter)

    }

    @UiThread
    override fun showError() {
        longSnackbar(myProfile_cl, "NETWORK ERROR")
    }

    private val onPhotoItemClick = OnItemClickListener { item, view ->
        if (item is PhotosProfileAdapterItem) {
            val action = MyProfileFragmentDirections.actionMyProfileFragmentToPhotoDetailsEditFragment(item.photo.photoID)
            mNav.navigate(action)
        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.removeView()
    }

}

package ivan.ba.myride.view.profile.otherProfile

import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl
import ivan.ba.myride.data.reposetoryImpl.RepUserImpl

class OtherProfileModel(val repUser: RepUserImpl, val repCar: RepCarImpl, val repPhoto: RepPhotoImpl) : OtherProfileMVP.Model {

    override fun getPhotosByUserID(userID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        repPhoto.getPhotosByUserID(userID, onFinish)
    }

    override fun getPhotosByCarID(carID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        repPhoto.getPhotosByCarID(carID, onFinish)
    }

    override fun getCarsByUserID(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit) {
        repCar.getCarsByUserID(userID, onFinish)
    }

}
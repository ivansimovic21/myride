package ivan.ba.myride.view.profile.myProfile

import ivan.ba.myride.data.dataModels.dmCar.DMCarProfile
import ivan.ba.myride.data.dataModels.dmPhoto.DMPhotoExplore
import ivan.ba.myride.data.dataModels.dmProfile.DMProfile
import ivan.ba.myride.data.reposetoryImpl.RepCarImpl
import ivan.ba.myride.data.reposetoryImpl.RepPhotoImpl
import ivan.ba.myride.data.reposetoryImpl.RepProfileImpl

class MyProfileModel(val repProfile: RepProfileImpl, val repCar: RepCarImpl, val repPhoto: RepPhotoImpl) : MyProfileMVP.Model {

    override fun getPhotosByUserID(userID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        repPhoto.getPhotosByUserID(userID, onFinish)
    }

    override fun getPhotosByCarID(carID: String, onFinish: (ArrayList<DMPhotoExplore>?) -> Unit) {
        repPhoto.getPhotosByCarID(carID, onFinish)
    }

    override fun getCarsByUserID(userID: String, onFinish: (ArrayList<DMCarProfile>?) -> Unit) {
        repCar.getCarsByUserID(userID, onFinish)
    }

    override fun getProfileByID(userID: String, onFinish: (DMProfile?) -> Unit) {
        repProfile.getProfileByID(userID, onFinish)
    }

}
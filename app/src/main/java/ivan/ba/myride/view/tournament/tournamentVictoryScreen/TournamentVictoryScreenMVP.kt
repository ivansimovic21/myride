package ivan.ba.myride.view.tournament.tournamentVictoryScreen

interface TournamentVictoryScreenMVP {
    interface View {

    }

    interface Presenter {

    }

    interface Model {

    }
}
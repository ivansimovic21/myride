package ivan.ba.myride

import android.app.Application
import android.content.Context
import ivan.ba.myride.data.fireStore.FireStoreModule
import ivan.ba.myride.inject.*
import ivan.ba.myride.view.car.CarModule
import ivan.ba.myride.view.chat.ChatModule
import ivan.ba.myride.view.photos.PhotoModule
import ivan.ba.myride.view.profile.ProfileModule
import ivan.ba.myride.view.tournament.TournamentModule
import ivan.ba.myride.view.user.UserModule
import javax.inject.Inject

class MyApp : Application() {
    companion object {
        //platformStatic allow access it from java code
        @JvmStatic
        lateinit var graph: ApplicationComponent
        lateinit var graphRepository: RepositoryComponent
        lateinit var instance: MyApp

    }

    fun getContext(): Context {
        return applicationContext
    }

    init {
        instance = this
    }

    @Inject
    override fun onCreate() {
        super.onCreate()
        graph = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .userModule(UserModule())
                .carModule(CarModule())
                .chatModule(ChatModule())
                .photoModule(PhotoModule())
                .profileModule(ProfileModule())
                .tournamentModule(TournamentModule())
                .build()

        graphRepository = DaggerRepositoryComponent.builder().fireStoreModule(FireStoreModule()).build()
    }
}
package ivan.ba.myride.inject

import android.arch.lifecycle.ReportFragment
import dagger.Component
import ivan.ba.myride.view.car.CarModule
import ivan.ba.myride.view.car.carDetails.CarDetailsFragment
import ivan.ba.myride.view.car.carEdit.CarEditFragment
import ivan.ba.myride.view.car.carFilter.CarFilterFragment
import ivan.ba.myride.view.car.carPhotosForExplore.CarPhotosForExploreFragment
import ivan.ba.myride.view.car.carPhotosForSale.CarPhotosForSaleFragment
import ivan.ba.myride.view.car.carsOfUser.CarsOfUserFragment
import ivan.ba.myride.view.car.carsOnExplore.CarsOnExploreFragment
import ivan.ba.myride.view.car.carsOnSale.CarsOnSalePresenter
import ivan.ba.myride.view.chat.ChatModule
import ivan.ba.myride.view.chat.chat.ChatFragment
import ivan.ba.myride.view.chat.contacts.ContactsFragment
import ivan.ba.myride.view.photos.PhotoModule
import ivan.ba.myride.view.photos.bookmarks.BookmarksFragment
import ivan.ba.myride.view.photos.comments.CommentsFragment
import ivan.ba.myride.view.photos.editPhoto.EditPhotoFragment
import ivan.ba.myride.view.photos.photoDetails.PhotoDetailsFragment
import ivan.ba.myride.view.photos.photoDetailsEdit.PhotoDetailsEditFragment
import ivan.ba.myride.view.photos.photoEdit.PhotoEditFragment
import ivan.ba.myride.view.photos.photoExplore.PhotoExploreFragment
import ivan.ba.myride.view.photos.photoSubscription.PhotoSubscriptionFragment
import ivan.ba.myride.view.photos.selectCarForPhoto.CarsForPhotoFragment
import ivan.ba.myride.view.profile.ProfileModule
import ivan.ba.myride.view.profile.myProfile.MyProfileFragment
import ivan.ba.myride.view.profile.otherProfile.OtherProfileFragment
import ivan.ba.myride.view.profile.profilePictures.ProfilePhotoFragment
import ivan.ba.myride.view.tournament.TournamentModule
import ivan.ba.myride.view.tournament.tournamentAddPhoto.TournamentAddPhotoFragment
import ivan.ba.myride.view.tournament.tournamentPhotoList.TournamentPhotoListFragment
import ivan.ba.myride.view.tournament.tournamentVictoryScreen.TournamentVictoryScreenFragment
import ivan.ba.myride.view.user.UserModule
import ivan.ba.myride.view.user.settings.SettingsFragment
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, UserModule::class, ProfileModule::class, PhotoModule::class, ChatModule::class, CarModule::class, TournamentModule::class))
interface ApplicationComponent {
    fun inject(target: SettingsFragment)
    fun inject(target: MyProfileFragment)
    fun inject(target: OtherProfileFragment)
    fun inject(target: PhotoDetailsFragment)
    fun inject(target: PhotoDetailsEditFragment)
    fun inject(target: PhotoEditFragment)
    fun inject(target: PhotoExploreFragment)
    fun inject(target: PhotoSubscriptionFragment)
    fun inject(target: ChatFragment)
    fun inject(target: ContactsFragment)
    fun inject(target: CarDetailsFragment)
    fun inject(target: CarEditFragment)
    fun inject(target: CarPhotosForExploreFragment)
    fun inject(target: CarPhotosForSaleFragment)
    fun inject(target: CarsOfUserFragment)
    fun inject(target: CarsOnExploreFragment)
    fun inject(target: CarsOnSalePresenter)
    fun inject(target: CarFilterFragment)
    fun inject(target: BookmarksFragment)
    fun inject(target: ReportFragment)
    fun inject(target: TournamentPhotoListFragment)
    fun inject(target: TournamentAddPhotoFragment)
    fun inject(target: TournamentVictoryScreenFragment)
    fun inject(target: EditPhotoFragment)
    fun inject(target: CarsForPhotoFragment)
    fun inject(target: CommentsFragment)
    fun inject(target: ProfilePhotoFragment)


}
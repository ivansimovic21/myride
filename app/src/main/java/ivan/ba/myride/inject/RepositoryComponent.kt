package ivan.ba.myride.inject

import dagger.Component
import ivan.ba.myride.data.fireStore.FireStoreModule
import ivan.ba.myride.data.reposetory.*
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(FireStoreModule::class))
interface RepositoryComponent {
    fun inject(target: RepCar)
    fun inject(target: RepChat)
    fun inject(target: RepPhoto)
    fun inject(target: RepTournament)
    fun inject(target: RepUser)
    fun inject(target: RepProfilePhoto)
    fun inject(target: RepProfile)
}